<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $path = strip_tags($get_company_details['clogo']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
            $curD = date('Y-m-d');


            ?>
            <!DOCTYPE html>
            <html lang="en">


            <head>
                <?php require('head.php'); ?>
                <script type="text/javascript">
                    $(function () {  //  document.ready
// Get Item Name from product table   
                        $("#itemName").on("change", function () {
                            var productId = $(this).val();
                            //alert ($productId);
                            $.ajax({
                                url: "result.php",
                                type: "POST",
                                data: {
                                    pid: $(this).val()
                                },
                                success: function (data) {

                                    //alert(data);itemVatType
                                    var vT = data;
                                    var vType = vT.substr(vT.indexOf("+") + 1);
                                    var vat = data;
                                    vat = vat.substring(0, vat.indexOf(','));
                                    var uP = data;
                                    var uPrice = uP.substr(uP.indexOf(",") + 1);
                                    var untPrc = uPrice.substring(0, uPrice.indexOf('+'));

                                    $("input[id ^='itemUnit']").val(untPrc);
                                    $("input[id ^='itemVat']").val(vat);
                                    $("input[id ^='itemVatType']").val(vType);

                                }

                            });

                        });

                        // Get min and max order quantity from product table

                        $("#itemQty").on("focusout", function () {
                            var itemQty = $(this).val();
                            var productId = $("select[id ^='itemName']").val();

                            $.ajax({
                                url: "resultValidate.php",
                                type: "POST",
                                data: {pid: productId, qty: itemQty},

                                success: function (data) {
                                    // $("#results").html(data);

//			   x = data;
                                    // alert(x);
//			   if (x == 1)
//				   {
//					  alert('Purchase Order Quantity is Less Than Minimum Order Quantity'); 
//					  $('#itemQty').val('0').focus();
//					  $('#btnadd').hide();
//				   }
//			   else if (x == 2)
//				   {
//					   alert('Purchase Order Quantity is Less Than Minimum Order Quantity'); 
//					  $('#itemQty').val('0').focus();
//					  $('#btnadd').hide();
//				   }
//			   else
//				   {
//					   $('#btnadd').show();
//				   }
                                    //elseif (x = 0)
                                }

                            });


                        });

                        $("#itemDis").on("focusout", function () {
                            var iDis = parseFloat($(this).val());
                            var subTot = parseFloat($("input[id ^='subTotal']").val());

                            //	alert(iDis+'--'+subTot)

                            if (iDis >= subTot) {
                                alert('UnAuthorised Discount Amount');
                                $('#itemDis').val('0').focus();
                                var iQty = $("input[id ^='itemQty']").val();
                                var iUnit = $("input[id ^='itemUnit']").val();
                                var iTot = parseFloat(iQty * iUnit).toFixed(2);
                                $('#subTotal').val(iTot);

                            }
                        });
                    });
                </script>

                <script>
                    $(document).ready(function () {
                        var $trCount = 0;
                        var $tdNum = 1;
                        var sum = 0;
                        var discount = 0;
                        var vatFreeTotal = 0;
                        var vatAble = 0;
                        var vatChrg = 0;
                        var GTotal = 0;
                        //var vat = 0;
                        //$addBtn = $('#btnadd');
                        $('#vat').change(function () {

                            if ($(this).attr('checked')) {
                                $(this).val('1');
                            } else {
                                $(this).val('0');
                            }

                            //	alert($(this).val());

                        });

                        $('#btnadd').click(function () {

                            var str = $('#itemName').val();
                            //str.substring(str.indexOf(".") + 1);
                            var $iName = str.substring(str.indexOf(".") + 1);

                            var s = $('#itemName').val();
                            var $iId = s.substring(0, s.indexOf('.'));

                            var barcodeinvoice = '<svg id="barcodeOutput" style="display: none"></svg>';


                            var $iQty = $('#itemQty').val();
                            var $iUnit = $('#itemUnit').val();
                            var $iDis = $('#itemDis').val();
                            var $iVat = $('#vat').val();
                            $iVatValue = $('#itemVat').val();
                            var $iSub = $('#subTotal').val();
                            var $newRow = '<tr><td id="iname' + $tdNum + '">' + $iName + '<input type="hidden" name="iiname[]' + $tdNum + '" value="' + $iId + '"/></td><td id="iqty' + $tdNum + '">' + $iQty + '<input type="hidden" name="iiqty[]' + $tdNum + '" value="' + $iQty + '"/></td><td id="iunit' + $tdNum + '">' + $iUnit + '<input type="hidden" name="iiunit[]' + $tdNum + '" value="' + $iUnit + '"/></td><td id="idis' + $tdNum + '">' + $iDis + '<input type="hidden" name="iidis[]' + $tdNum + '" value="' + $iDis + '"/></td><td id="isubtotal' + $tdNum + '" class="tPrice">' + $iSub + '<input type="hidden" id="st" name="iisubTotal' + $tdNum + '" value="' + $iSub + '"/><input type="hidden" id="ivat" name="iivat[]' + $tdNum + '" value="' + $iVat + '"/><input type="hidden" id="ivatValue" name="iivatValue[]' + $tdNum + '" value="' + $iVatValue + '"/></td><td><a href="#" class="btn" id="detBtn">Delete</td></tr>';


                            $('table#itemDetails').append($newRow);
                            $trCount += 1;
                            $tdNum += 1;
                            $('span#trCounter').html($trCount);
                            $('input[name=hdRCount]').val($trCount);


                            $('.tPrice').each(function () {
                                calculateSum();

                            });

                            //$('#vat').on('click', function () {
//			$(this).val(this.checked ? 1 : 0);
//			console.log($(this).val());
//			});


                            // alert(sum);
                            getValues();
                            $('#itemQty').val('');
                            $('#itemUnit').val('');
                            $('#itemDis').val(0);
                            $('#subTotal').val('');


                        });


//subGTotal=0;


                        $('a#detBtn').live('click', function () {
                            $(this).parent().parent().fadeOut('slow', function () {
                                $(this).remove();

                                $('.tPrice').each(function () {
                                    calculateSum();

                                });

                            })
                            $trCount -= 1;
                            $tdNum -= 1;
                            $('span#trCounter').html($trCount);
                            $('input[name=hdRCount]').val($trCount);
                        });
                        $('#form').submit(function () {
                            if ($("#trCounter").html() == 0 || $('#itemName').val() == '' || $('#billAmt').val() == '' || $('#bilPmt').val() == '') {
                                alert('you did not fill out one of the fields');
                                return false;
                            }
                        });


                    });
                </script>


            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <section class="panel">
                            <header class="panel-heading">
                                Select Product For Invoice
                            </header>
                            <div class="panel-body">
                                <form class="mws-form" id="frm" action="invExc.php" method="post"
                                      enctype="multipart/form-data">
                                    <div class="adv-table editable-table ">
                                        <div class="clearfix">

                                            <!--  <button id="editable-sample_new" class="btn green">
                                                  Add New <i class="fa fa-plus"></i>
                                              </button>-->

                                            <p>Select Customer
                                                <?php
                                                $query = "SELECT * FROM customermaster";
                                                $result = mysql_query($query);

                                                echo ' <select id="cusId" name="cusId" class="form-control" style="width:50%">';

                                                //$thisCat = NULL;
                                                while ($row = mysql_fetch_assoc($result)) {

                                                    echo '<option value="' . $row['customerId'] . '">&nbsp;&nbsp;' . htmlspecialchars($row['customerName']) . '</option>';
                                                }

                                                echo "</select>"; ?>
                                            </p>

                                            <!--<div class="btn-group pull-right">
                                                <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#">Print</a></li>
                                                    <li><a href="#">Save as PDF</a></li>
                                                    <li><a href="#">Export to Excel</a></li>
                                                </ul>
                                            </div>-->
                                        </div>
                                        <div class="space15"></div>
                                        <div id="itemResult" class="mws-form-inline">
                                            <table class="table">

                                                <tr>
                                                    <td><label class="control-label" id="iName">Item</label></td>
                                                    <td>
                                                        <?php
                                                        $query = "SELECT * FROM product_t";
                                                        $result = mysql_query($query);

                                                        echo ' <select id="itemName" name="itemName" class="form-control">';

                                                        //$thisCat = NULL;
                                                        while ($row = mysql_fetch_assoc($result)) {

                                                            echo '<option value="' . $row['id'] . '.' . $row['productName'] . '">&nbsp;&nbsp;' . htmlspecialchars($row['productName']) . '</option>';
                                                        }

                                                        echo "</select>"; ?><input type="hidden" id="itemVat"
                                                                                   name="itemVat" value=""
                                                                                   class="form-control small"/>
                                                    </td>


                                                    <td><label class="control-label">Quantity</label></td>
                                                    <td><input type="text" class="form-control small" id="itemQty"
                                                               numeric name="itemQty" class="small"
                                                               onKeyUp="getValues()" onBlur=""/></td>
                                                    <td><label class="control-label">Price</label></td>
                                                    <td><input type="text" id="itemUnit" name="itemUnit"
                                                               class="form-control small" readonly/></td>
                                                    <td><label class="control-label">Discount</label></td>
                                                    <td><input type="text" id="itemDis" name="itemDis"
                                                               class="form-control small" onKeyUp="getValues()"
                                                               onBlur="" value="0"/></td>
                                                    <td><label class="control-label">Vat</label></td>
                                                    <td><input id="vat" type="checkbox" name="vat"/><input type="hidden"
                                                                                                           id="itemVatType"
                                                                                                           name="itemVatType"
                                                                                                           value=""
                                                                                                           class="form-control small"/>
                                                    </td>
                                                    <td><label class="control-label">Total</label></td>

                                                    <td><input type="text" id="subTotal" name="subTotal"
                                                               class="form-control small" readonly/></td>

                                                    <td><input type="button" id="btnadd" name="btnadd" class="btn"
                                                               value="Add Item"/></td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div id="itemResult" class="mws-form-inline">
                                            <table id="itemDetails"
                                                   class="table table-striped table-hover table-bordered">
                                                <tr>
                                                    <th scope="col">Item Name</th>
                                                    <th scope="col">Quantity</th>
                                                    <th scope="col">Unit</th>
                                                    <th scope="col">Discount</th>
                                                    <th scope="col">Total</th>
                                                    <th scope="col"></th>
                                                    <th scope="col">Barcode</th>
                                                </tr>
                                            </table>
                                        </div>


                                        <div class="text-center invoice-btn">
                                            <!--     <a class="btn btn-danger btn-lg"><i class="fa fa-check"></i> Purchase </a> -->
                                            <!-- <a class="btn btn-info btn-lg" onclick="javascript:window.print();"><i class="fa fa-print"></i> Submit </a>-->
                                            <input type="hidden" id="hdCount" name="hdRCount" value="0"/>
                                            <input type="submit" id="btnsubmit" name="btnsubmit" value="Submit"
                                                   class="btn btn-danger btn-lg"/>
                                        </div>
                                </form>
                            </div>
                        </section>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>


            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>