<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo


            ?>

            <!DOCTYPE html>
            <html lang="en">

            <head>
                <?php require('head.php'); ?>
                <style>
                    .wrap {
                        width: 700px;
                        margin: 10px auto;
                        padding: 10px 15px;
                        background: white;
                        border: 2px solid #DBDBDB;
                        -webkit-border-radius: 5px;
                        -moz-border-radius: 5px;
                        border-radius: 5px;
                        text-align: center;
                        overflow: hidden;
                    }
                    #preview {
                        color: red;
                    }
                    #preview img {
                        margin-top: 30px;
                        max-width: 100%;
                        border: 0;
                        border-radius: 3px;
                        -webkit-box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
                        box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
                        overflow: hidden;
                    }

                    input[type="submit"] {
                        border-radius: 10px;
                        background-color: #61B3DE;
                        border: 0;
                        color: white;
                        font-weight: bold;
                        font-style: italic;
                        padding: 6px 15px 5px;
                        cursor: pointer;
                    }
                </style>

                <?php require('head.php'); ?>
            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo -->
                <section id="main-content">
                    <section class="wrapper">
                        <!-- page start-->
                        <div class="row">
                            <aside class="profile-info col-lg-6">

                                <section>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading"> Update Company Logo</div>
                                        <div class="panel-body">

                                            <p>Valid formats: jpeg, gif, png, Max upload: 200kb (140px X 140px)</p>
                                            <!-- loader.gif -->
                                            <img style="display:none" id="loader" src="loader.gif" alt="Loading...."
                                                 title="Loading...."/>

                                            <form id="form-horizontal" role="form" action="ajaxupload.php" method="post"
                                                  enctype="multipart/form-data">

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">logo</label>
                                                    <div class="col-lg-6">
                                                        <input id="uploadImage" type="file" accept="image/*"
                                                               name="image"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <!--<button type="submit" class="btn btn-info">Update Logo</button>-->
                                                        <input id="button" class="btn btn-info" type="submit"
                                                               value="Upload Logo">
                                                        <button type="button" class="btn btn-default">Cancel</button>
                                                    </div>
                                                </div>

                                                <div id="preview" style="display:none"></div>
                                            </form>
                                        </div>
                                    </div>
                                </section>
                            </aside>
                            <aside class="profile-info col-lg-6">
                                <section class="panel panel-primary">
                                    <div class="panel-heading">
                                        Update Company Information
                                    </div>
                                    <div class="panel-body bio-graph-info">
                                        <h1> Company Information</h1>
                                        <form class="form-horizontal" role="form" action="cEpro.php" method="post">

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Company Name</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="companyName"
                                                           name="companyName" value="<?php echo $companyName; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Reg No</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="regNo" name="regNo"
                                                           value="<?php echo $regNo; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Phone No</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="phoneNo" name="phoneNo"
                                                           value="<?php echo $phoneNo; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Mobile No</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="mobileNo"
                                                           name="mobileNo" value="<?php echo $mobileNo; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Fax No</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="faxNo" name="faxNo"
                                                           value="<?php echo $faxNo; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Address</label>
                                                <div class="col-lg-6">
                                                    <textarea name="address" id="address" class="form-control" cols="30"
                                                              rows="10"
                                                              name="address"><?php echo $address; ?></textarea>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">City</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="city" name="city"
                                                           value="<?php echo $city; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Country</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="country" name="country"
                                                           value="<?php echo $country; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Company Email</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="comEmail"
                                                           name="comEmail" value="<?php echo $comEmail; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">Website URL</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="comWeb" name="comWeb"
                                                           value="<?php echo $comWeb; ?>">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="submit" class="btn btn-success">Save</button>
                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>

                            </aside>

                        </div>

                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>

            <script type="text/javascript" src="js/jquery.form.js"></script>
            <script type="text/javascript" src="js/script.js"></script>
            <script>

                //owl carousel

                $(document).ready(function () {
                    $("#owl-demo").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        autoPlay: true

                    });
                });

                //custom select box

                $(function () {
                    $('select.styled').customSelect();
                });

            </script>

            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>