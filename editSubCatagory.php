<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo


            ?>


            <!DOCTYPE html>
            <html lang="en">

            <head>
                <?php require('head.php'); ?>
            </head>

            <!--Updating the category name-->
            <?php

            //Receiving category id from category
            $scid = $_GET['id'];

            //Getting catagory name from catagoty
            $result_scat = mysql_query("SELECT * FROM  subcategory WHERE id='$scid'");
            $row_scat = mysql_fetch_row($result_scat);
            $scat_name = $row_scat[1];


            ?>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo -->
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <section class="panel">
                            <header class="panel-heading tab-bg-dark-navy-blue tab-right ">

                                <span class="hidden-sm wht-color">Update sub category</span>
                            </header>


                            <div class="panel-body">
                                <form role="form" class="form-inline" action="updateSubCategory.php" method="post">

                                    <div class="form-group">
                                        <label for="exampleInputEmail2" class="sr-only">Sub category Name</label>
                                        <input type="hidden" name="scid" id="scid" value="<?php echo $scid; ?>">
                                        <input type="text" value="<?php echo $scat_name; ?>" id="subCatName"
                                               name="subCatName" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail2" class="sr-only">Sub Category Name</label>
                                        <?php
                                        $query = "SELECT * FROM catgory";
                                        $result = mysql_query($query);

                                        echo ' <select name="catId" class="form-control">';

                                        //$thisCat = NULL;
                                        while ($row = mysql_fetch_assoc($result)) {

                                            echo '  <option value="' . $row['id'] . '">&nbsp;&nbsp;' . htmlspecialchars($row['catName']) . '</option>';
                                        }

                                        echo "</select>"; ?>

                                    </div>


                                    <button class="btn btn-success" type="submit">Update sub category</button>
                                </form>


                            </div>
                        </section>


                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>

            <script>

                //owl carousel

                $(document).ready(function () {
                    $("#owl-demo").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        autoPlay: true

                    });
                });

                //custom select box

                $(function () {
                    $('select.styled').customSelect();
                });

            </script>

            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>