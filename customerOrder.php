<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID']))
{
	//$user= $_SESSION['SESS_MEMBER_ID'];
	//Check the database table for the logged in user information
	$check_user_details = mysql_query("select * from user where userId = '".mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"])."'");
	//Validate created session
	if(mysql_num_rows($check_user_details) < 1)
	{
		//echo 'Not in Member List';echo '<br>';
		session_unset();
		session_destroy();
		header("location: login.php");
	}
	elseif(mysql_num_rows($check_user_details) > 0)
	{
		//echo 'Member';echo '&nbsp;&nbsp;';
		$get_user_details = mysql_fetch_array($check_user_details);
		$role = strip_tags($get_user_details['role']);
		//echo $role;
		if($role!=3)
			{
				//echo 'But Not Authorised';echo '<br>';
				header("location: error.php");
				exit(); 
			}
			else
			{
				//echo 'Authorised';echo '<br>';
	
				//Get all the logged in user information from the database users table
				//$get_user_details = mysql_fetch_array($check_user_details);
				//echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status 
				require_once('auth.php');
				$fname = strip_tags($get_user_details['userFname']);
				$lname = strip_tags($get_user_details['userLname']);
				$mobileNo=strip_tags($get_user_details['mobileNo']);
				$userId = strip_tags($get_user_details['userId']);
				
				$role = strip_tags($get_user_details['role']);
				$barnchId=strip_tags($get_user_details['barnchId']);
 				$companyId=strip_tags($get_user_details['companyId']);
				
				
				
				
				$check_company_details = mysql_query("select * from company");
				$get_company_details = mysql_fetch_array($check_company_details);
			
				$companyName = strip_tags($get_company_details['companyName']);
				$ownerName = strip_tags($get_company_details['ownerName']);
				$phoneNo=strip_tags($get_company_details['phoneNo']);
				$regNo=strip_tags($get_company_details['regNo']);
				$mobileNo = strip_tags($get_company_details['mobileNo']);
			
				$faxNo = strip_tags($get_company_details['faxNo']);
				$address = strip_tags($get_company_details['address']);
				$city=strip_tags($get_company_details['city']);
				$country = strip_tags($get_company_details['country']);
				$path = strip_tags($get_company_details['clogo']);
				$comEmail=strip_tags($get_company_details['comEmail']);
				$comWeb = strip_tags($get_company_details['comWeb']);
				
		//	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
				$curD = date('Y-m-d');
				
  				
?>
<!DOCTYPE html>
<html lang="en">
  

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Taibur">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Online Sales And Inventory Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
 <script src="js/jquery-1.8.3.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript">
    $(function() {  //  document.ready
// Get Item Name from product table   
    $("#itemName").on("change", function() {
	var productId = $(this).val();
	//alert ($productId);
        $.ajax({
            url: "result.php",
            type: "POST",
            data: {
                pid: $(this).val()
            },
            success: function(data) {
				
			  var vat = data;
				vat = vat.substring(0, vat.indexOf(','));
				var uP = data;
				var uPrice = uP.substr(uP.indexOf(",") + 1);
			//   $("input[id ^='itemUnit']").val(uPrice);
			 //  $("input[id ^='itemVat']").val(vat);
			   
            }
		
        });
	
    });

	// Get min and max order quantity from product table   
   	
	$("#itemQty").on("focusout", function() {
		var itemQty = $(this).val();
		var productId = $("select[id ^='itemName']").val();

		$.ajax({
            url: "resultValidate.php",
            type: "POST",
            data: { pid: productId, qty: itemQty },
		
			success: function(data) {
               // $("#results").html(data);
			   
			   x = data;
			 // alert(x); 
			   if (x == 1)
				   {
					  alert('Purchase Order Quantity is Less Than Minimum Order Quantity'); 
					  $('#itemQty').val('0').focus();
					  $('#btnadd').hide();
				   }
			   else if (x == 2)
				   {
					   alert('Purchase Order Quantity is Less Than Maximum Order Quantity'); 
					  $('#itemQty').val('0').focus();
					  $('#btnadd').hide();
				   }
			   else
				   {
					   $('#btnadd').show();
				   }
			   //elseif (x = 0)
            }
            
		});
		      
	
    });
	
	
	
	$("#subVatTotal").on("focusout", function() {
	//	purchaseVat = 0;
		var purchaseVat = parseInt($(this).val());
		var SubTotal = parseInt($("input[id ^='subGTotal']").val());
		var GrandTotal = parseInt((SubTotal+purchaseVat)).toFixed(2);
		//alert(purchaseVat+','+SubTotal+','+GrandTotal);
		$("input[id ^='GTotal']").val(GrandTotal).toFixed(2);
		
		
	});

	
});
</script> 

<script type="text/javascript">
function tot(elem) 
	{
	var d=document.getElementById("subGTotal").value;
	var subGTotal=Number(d); 
	
	}	
	
// var subGTotal=0;
var suTotal=0;

function getValues() 
{
	var qty1 = 0;
	var unit = 0;
	var pvat = 0;
	var ipVat = 0;
var provat = 0;



   var obj = document.getElementsByTagName("input");
  
  for(var i=0; i<obj.length; i++)
  		{
     			
			
			if(obj[i].name == "itemQty")
				{
					var qty1 = obj[i].value;
				}
				
     		if(obj[i].name == "itemUnit")
				{
					var unit = obj[i].value;
				}
				if(obj[i].name == "itemVat")
				{					
					var ipVat = obj[i].value;
				}
			
			if ( obj[i].name == 'vat' ) 
							{
								
								if ( obj[i].type === 'checkbox' ) 
									 {
										
										if ( obj[i].checked ) 
											{
												var protot = (qty1*unit);
												
												var pvat = (protot*ipVat)/100;
												
											} else {
												
												var pvat = 0;
											}
									 }
							}
			
			
				
			if(obj[i].name == "subTotal")
			  {
							if(qty1 > 0 && unit > 0)
						 {
							 obj[i].value = (qty1*unit+pvat).toFixed(2);  //  sub total
                             subGTotal+=(obj[i].value*1);   // Grand Tota
							//suTotal+=subGTotal;
							//alert(suTotal)
						 }
						else
						{
							obj[i].value = 0;
							subGTotal+=(obj[i].value*1);
							// alert(subGTotal+'2')
							
						}
				}
         }  // for loop end
		
}

function calculateSum() {
 
        var sum = 0;
        //iterate through each textboxes and add the values
       $(".tPrice").each(function() {

			var value = $(this).text();
			// add only if the value is number
			if(!isNaN(value) && value.length != 0) {
				sum += parseFloat(value);
				//alert(sum);
			}
			
		//	document.getElementById("sum").value = sum.toFixed(2);
		});	
        //.toFixed() method will roundoff the final sum to 2 decimal places
        $("#subGTotal").html(sum.toFixed(2));
	//	document.getElementById("subGTotal").value = sum.toFixed(2);
		$("input[id ^='subGTotal']").val(sum.toFixed(2));
		$("input[id ^='GTotal']").val(sum.toFixed(2));
    }
	
</script>



<script>
$(document).ready(function(){
	var $trCount=0;
	var $tdNum=1;
	var sum = 0;
	

	// iterate through each td based on class and add the values
//$("#chk").click(function() {
//    $(".hk").each(function() {
//        alert(this.value);
//    });
//});
$("#chk").click(function() {
	//alert('ksdjfsdjk');
		$('#frm input[type="text"]').each(function(){
				// Do your magic here
				alert(this.value);
		});

});
//$("#sum").html(sum.toFixed(2));	

	//$addBtn = $('#btnadd');
	$('#btnadd').click(function(){
		
		var str = $('#itemName').val();
		//str.substring(str.indexOf(".") + 1);
		var $iName = str.substring(str.indexOf(".") + 1);
		
		var s = $('#itemName').val();
		var $iId = s.substring(0, s.indexOf('.'));

		var $iQty = $('#itemQty').val();
		var $iUnit = $('#itemUnit').val();
		var $iSub = $('#subTotal').val();
		
		var $newRow = '<tr><td id="iname'+$tdNum+'">'+$iName+'<input type="hidden" name="iiname[]'+$tdNum+'" value="'+$iId+'"/></td><td id="iqty'+$tdNum+'">'+$iQty+'<input type="hidden" name="iiqty[]'+$tdNum+'" value="'+$iQty+'"/></td><td id="iunit'+$tdNum+'">'+$iUnit+'<input type="hidden" name="iiunit[]'+$tdNum+'" value="'+$iUnit+'"/></td><td id="isubtotal'+$tdNum+'" class="tPrice">'+$iSub+'<input type="hidden" id="st" name="iisubTotal[]'+$tdNum+'" value="'+$iSub+'"/></td><td><a href="#" class="btn" id="detBtn">Delete</td></tr>';

		
		
		
		
		
		$('table#itemDetails').append($newRow);
		$trCount+=1;
		$tdNum+=1;	
		$('span#trCounter').html($trCount);
		$('input[name=hdRCount]').val($trCount);
		$('#itemQty').val('');
		$('#itemUnit').val('');
		$('#subTotal').val('');
		
		// sum = sum + $('#st').val();
	var tt1="itemDetails";
	    //	sum = ($sum + $('#st').val());
		//sum += parseInt($('#st').val());
		// alert(sum);
		getValues();
		
		$('.tPrice').each(function() {
        		calculateSum();
		
    		});	
			getGValues();
		
	});
	
	
//subGTotal=0;

	
	
	
	$('a#detBtn').live('click',function(){
		
			
		$(this).parent().parent().fadeOut('slow',function(){$(this).remove();
		
		$('.tPrice').each(function() {
        		calculateSum();
		
    		});
			//alert('dfsf');
		
		})
		
		
		$trCount-=1;
		$tdNum-=1;	
		$('span#trCounter').html($trCount);
		$('input[name=hdRCount]').val($trCount);
		
			
		
	});
	
	
});

	
	
	
	
</script>


    
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header white-bg">
            <?php include ("header.php");?>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <?php include("menu.php"); ?>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
              <!-- page start-->
              <section class="panel">
                  <header class="panel-heading">
                      Stock List Entry ( Purchase Product From Vendor)
                  </header>
                  <div class="panel-body">
                  
                  <div class="row invoice-list">
                              <form class="mws-form" id="frm" action="poExc.php" method="post" enctype="multipart/form-data">
                              <div class="col-lg-4 col-sm-4">
                                  <h4>SUPPLIER INFO</h4>
                                  <p>
                                      <?php
                                     $query = "SELECT * FROM suppliermaster";
										$result = mysql_query($query);
										
										echo ' <select id="supId" name="supId" class="form-control">';
										
										//$thisCat = NULL;
										while ($row = mysql_fetch_assoc($result)) {
										 
										  echo '<option value="'.$row['supplierId'].'">&nbsp;&nbsp;'.htmlspecialchars($row['companyName']).'</option>';
										}
										
										echo "</select>";?>
                                  </p>
                            
                             
                            
                          </div>
                  
                      <div class="adv-table editable-table">
                          <div class="clearfix">
                              <div class="btn-group">
                                
                              </div>
                              <div class="btn-group pull-right">
                               
                                  <!--<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i></button>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a href="#">Print</a></li>
                                      <li><a href="#">Save as PDF</a></li>
                                      <li><a href="#">Export to Excel</a></li>
                                  </ul>-->
                              </div>
                          </div>
                          <div class="space15"></div>
                         <div id="itemResult">
                                <table class="table">
                          
                                    
                                    <tr>
                                      <td><label class="control-label" id="iName">Item</label></td>
                                      <td>
                                       <?php
                                     $query = "SELECT * FROM product_t";
										$result = mysql_query($query);
										
										echo ' <select id="itemName" name="itemName" class="form-control">';
										
										//$thisCat = NULL;
										while ($row = mysql_fetch_assoc($result)) {
										 
										  echo '<option value="'.$row['id'].'.'.$row['productName'].'">&nbsp;&nbsp;'.htmlspecialchars($row['productName']).'</option>';
										}
										
										echo "</select>";?><input type="hidden" id="itemVat" name="itemVat" value="" class="form-control small" />
                                       </td>
                                      
                                      
                                      
                                      
                                      <td><label class="control-label">Quantity</label></td>
                                      <td><input type="text" class="form-control small" id="itemQty" numeric name="itemQty" class="small" onKeyUp="getValues()" onBlur=""/></td>
                                      <td><label class="control-label">Price</label></td>
                                      <td><input type="text" id="itemUnit" name="itemUnit" class="form-control small" onKeyUp="getValues()" onBlur=""/></td>
                                       
                                       <td><label class="control-label">Total</label></td>
                                        
                                      <td><input type="text" id="subTotal" name="subTotal" class="form-control small" readonly/></td>
                                     
                                      <td><input type="button" id="btnadd" name="btnadd" class="btn" value="Add Item" /></td>
                                    </tr>
                                </table>
                            </div>
                            
                            <div id="itemResult">
                                <table id="itemDetails" class="table table-hover table-bordered">
                                  <tr>
                                    <th scope="col">Item Name</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Unit</th>
                                    <th scope="col">Total</th>
                                    <th scope="col"></th>
                                  </tr>
                                </table>
                           </div>
                          
                         <div class="row">
                              <div class="col-lg-4 invoice-block pull-right">
                                  <ul class="unstyled amounts">
                                      <li><strong style="margin-top:8px; float:left">Sub - Total :</strong><input type="text" style="width:80%;" id="subGTotal" name="subGTotal" class="form-control small" value="" readonly /></li>
                                      <li><strong style="margin-top:8px; float:left">VAT :</strong> <input type="text" style="width:80%;"  id="subVatTotal" name="subVatTotal" class="form-control small" value="" onKeyUp="getGValues()" onBlur=""/></li>
                                      <li><strong style="margin-top:8px; float:left">Grand Total :</strong> <input type="text" style="width:80%;"  id="GTotal" name="GTotal" class="form-control small" value="" /></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="text-center invoice-btn">
                            
                            <input type="hidden" id="hdCount" name="hdRCount" value="0"/>
                            <input type="submit" id="btnsubmit" name="btnsubmit" value="Submit" class="btn btn-danger btn-lg"/>
                              
                              </form>
                          </div>
                     
                  </div>
              </section>
              <!-- page end-->
          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
       <?php include("footer.php");?>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.8.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
    <script src="js/respond.min.js" ></script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>

      <!--script for this page only-->
      

      <!-- END JAVASCRIPTS -->
     

  </body>

</html>
<?php
	}
}

}
else
{
	header("location: login.php");
	exit(); 
}
	
?>