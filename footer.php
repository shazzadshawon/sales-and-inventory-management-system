<?php ?>

<footer class="site-footer">
	<div class="text-center">
		2018 &copy; Asian Global Ventures (BD) Ltd.
		<a href="#" class="go-top">
			<i class="fa fa-angle-up"></i>
		</a>
	</div>
</footer>