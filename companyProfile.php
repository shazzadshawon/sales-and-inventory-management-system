<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID']))
{
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '".mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"])."'");
    //Validate created session
    if(mysql_num_rows($check_user_details) < 1)
    {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    }
    elseif(mysql_num_rows($check_user_details) > 0)
    {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if($role!=3)
        {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        }
        else
        {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo=strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId=strip_tags($get_user_details['barnchId']);
            $companyId=strip_tags($get_user_details['companyId']);




            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo=strip_tags($get_company_details['phoneNo']);
            $regNo=strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city=strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $path = strip_tags($get_company_details['clogo']);
            $comEmail=strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo



            ?>

            <!DOCTYPE html>
            <html lang="en">

            <head>
                <?php require('head.php'); ?>
            </head>

            <body>

            <section id="container" >
                <!--header start-->
                <header class="header white-bg">
                    <?php include ("header.php");?>
                </header>
                <!--header end-->
                <!--Overlay Menu start-->
                <div>
                    <?php require("overlayMenu.php"); ?>
                </div>
                <!--Overlay Menu start-->
                <!--main content start //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo -->
                <section id="main-content">
                    <section class="wrapper">
                        <!-- page start-->
                        <div class="row">
                            <aside class="profile-nav col-lg-3">
                                <section class="panel">
                                    <div class="user-heading round">
                                        <a href="#">

                                            <img src="<?php echo $path; ?>" alt="">
                                        </a>
                                        <h1 style="font-size:16px"><?php echo $companyName;?></h1>
                                        <p>Reg No - <?php echo $regNo;?></p>

                                    </div>

                                    <ul class="nav nav-pills nav-stacked">

                                        <li class="active"><a href="companyEditProfile.php"> <i class="fa fa-edit"></i> Edit profile</a></li>
                                    </ul>

                                </section>
                            </aside>
                            <aside class="profile-info col-lg-9">

                                <section class="panel panel-primary">
                                    <div class="panel-heading">
                                        <?php echo $companyName;?> Details
                                    </div>
                                    <div class="panel-body bio-graph-info" style="padding-top:50px;">
                                        <div class="row">
                                            <div class="bio-row">
                                                <p><span>Mobile No </span>: <?php echo $mobileNo;?></p>
                                            </div>

                                            <div class="bio-row">
                                                <p><span>Address </span>: <?php echo $address;?></p>
                                            </div>
                                            <div class="bio-row">
                                                <p><span>Phone No</span>: <?php echo $phoneNo;?></p>
                                            </div>
                                            <div class="bio-row">
                                                <p><span>City</span>: <?php echo $city;?></p>
                                            </div>

                                            <div class="bio-row">
                                                <p><span>Fax No </span>: <?php echo $faxNo;?></p>
                                            </div>
                                            <div class="bio-row">
                                                <p><span>Country </span>: <?php echo $country;?></p>
                                            </div>

                                            <div class="bio-row">
                                                <p><span>Email </span>: <?php echo $comEmail;?></p>
                                            </div>
                                            <div class="bio-row">
                                                <p><span>Web </span>: <?php echo $comWeb;?></p>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            </aside>
                        </div>

                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php include("footer.php");?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>

            </body>

            </html>
            <?php
        }
    }

}
else
{
    header("location: login.php");
    exit();
}

?>