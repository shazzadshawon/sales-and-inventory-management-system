<?php
echo "<p><b>Example of using the Break statement:</b></p>";

for ($i=0; $i<=10; $i++) {
   if ($i==3){break;} 
   echo "The number is ".$i;
   echo "<br />";
}

echo "<p><b>One more example of using the Break statement:</b><p>";

$i = 0;
$j = 0;

while ($i < 10) {
  while ($j < 10) {
    if ($j == 5) {break 2;} // breaks out of two while loops
    $j++;
  }
  $i++;
}

echo "The first number is ".$i."<br />";
echo "The second number is ".$j."<br />";
?> 