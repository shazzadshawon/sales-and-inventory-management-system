<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $path = strip_tags($get_company_details['clogo']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
            $curD = date('Y-m-d');


            ?>
            <!DOCTYPE html>
            <html lang="en">


            <head>
                <?php require('head.php'); ?>

                <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css"/>
                <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css"/>
                <link rel="stylesheet" type="text/css" href="assets/bootstrap-timepicker/compiled/timepicker.css"/>

                <script>
                    $(document).ready(function () {
                        var $totDues = 0;
                        var $tdInd;
                        $('#selSupplier').on('change', function () {
                            var $val = $(this).val();
                            if ($val != '#') {
                                $.ajax({
                                    type: "POST",
                                    url: "ajaxSupplierDueList.php",
                                    data: "poid=" + $val,
                                    beforeSend: function () {
                                        $('.loader').show();
                                    },
                                    success: function (returnTable) {
                                        $('div#itemResult').html(returnTable);
                                        $totDues = 0;
                                        $('.loader').hide();
                                    }
                                });
                            }
                            else {
                                userBack();
                                $default = '<table id="poList" class="table table-hover table-bordered"><tr><th scope="col">#</th><th scope="col">Date</th><th scope="col">P.O. Number</th><th scope="col">PO Amount</th><th scope="col">VAT</th><th scope="col">Other Charges</th><th scope="col">Total Amt.</th><th scope="col">Paid Amt.</th><th scope="col">Dues</th></tr></table>';
                                $('div#itemResult').html($default);
                            }
                        });
                        //////////////////////////////////////
                        $('#tabCash, #tabCQ').click(function () {
                            $('#inpCashAmt').val('');
                            $('#inpChequeNumber').val('');
                            $('#inpChequeDate').val('');
                            $('#inpChequeBank').val('');
                            $('#inpChequeAmt').val('');
                        });
                        //////////////////////////////////////
                        $('#btnCashPayment').click(function () {
                            var $tdCashExist = $('#tdCash').length;
                            var $cashAmt = $('#inpCashAmt').val();
                            //var $remBalance = chkBalance();
                            if ($cashAmt <= chkBalance()) {
                                if ($tdCashExist == 0) {
                                    var $addCash = '<tr class="multiplePmts"><td id="tdCash">Cash<input type="hidden" name="pmtName[]" value="CASH" /></td><td id="tdCashAmt">' + $cashAmt + '<input type="hidden" name="iCQNo[]" value="" /><input type="hidden" name="iCQdt[]" value="" /><input type="hidden" name="iCQbank[]" value="" /><input type="hidden" name="pmtAmt[]" value="' + $cashAmt + '" /></td><td><button class="btn btn-white del" data-toggle="button"><i class="fa fa-times"></i></button></td></tr>';
                                    $('table#pmtList').append($addCash);
                                    $('#hdInpPaidAmt').val(calcPaidAmounts());
                                    $('#rcvdAmount').text(calcPaidAmounts());
                                }
                                else {
                                    $('#tdCashAmt').text($cashAmt);
                                    $('#hdInpPaidAmt').val(calcPaidAmounts());
                                    $('#rcvdAmount').text(calcPaidAmounts());
                                }
                            }
                        });
                        //////////////////////////////////////
                        $('#btnCQPayment').click(function () {
                            var $tdCQExist = $('.tdCQ').length;
                            var $cqNo = $('#inpChequeNumber').val();
                            var $cqAmt = $('#inpChequeAmt').val();
                            var $cqDt = $('#inpChequeDate').val();
                            var $cqBank = $('#inpChequeBank').val();
                            //var $remBalance = chkBalance();

                            if ($cqAmt <= chkBalance()) {
                                if (chequeExist($cqNo) == 'true') {
                                    alert('The cheque is already exist.');
                                }
                                else {
                                    var $addCQ = '<tr class="multiplePmts"><td class="tdCQ">Cheque<input type="hidden" name="pmtName[]" value="CHEQUE" /></td><td  id="tdCQAmt" class="tdCQAmt">' + $cqAmt + '<input type="hidden" name="iCQNo[]" value="' + $cqNo + '" /><input type="hidden" name="iCQdt[]" value="' + $cqDt + '" /><input type="hidden" name="iCQbank[]" value="' + $cqBank + '" /><input type="hidden" name="pmtAmt[]" value="' + $cqAmt + '" /></td><td><button class="btn btn-white del" data-toggle="button"><i class="fa fa-times"></i></button></td></tr>';
                                    $('table#pmtList').append($addCQ);
                                    $('#hdInpPaidAmt').val(calcPaidAmounts());
                                    $('#rcvdAmount').text(calcPaidAmounts());
                                }
                            }
                        });
                        ////////////////////////////////////// hdInpPayableAmt hdInpPaidAmt hdInpSelPOs
                        $('.selCheck').live('change', function () {
                            if ($(this).is(':checked')) {
                                $thisDue = $(this).parent().parent().find('td:eq(8)').text();
                                $totDues = parseFloat($totDues) + parseFloat($thisDue);
                                //alert($totDues);
                                $('#hdInpPayableAmt').val($totDues);
                                $('#hdInpSelPOs').val(calcSelectedDues());
                                $('#selAmountShow').text($totDues);
                            }
                            else {
                                $thisDue = $(this).parent().parent().find('td:eq(8)').text();
                                $totDues = parseFloat($totDues) - parseFloat($thisDue);
                                //alert($totDues);
                                $('#hdInpPayableAmt').val($totDues);
                                $('#hdInpSelPOs').val(calcSelectedDues());
                                $('#selAmountShow').text($totDues);
                                if ($('.multiplePmts').length > 0) {
                                    $('.multiplePmts').remove();
                                    $('#hdInpPaidAmt').val(0);
                                    $('#rcvdAmount').text('0.00');
                                }
                            }
                        });
                        //////////////////////////////////////
                        $('button.del').live('click', function () {
                            $(this).parent().parent().fadeOut('slow').remove();
                            $('#hdInpPaidAmt').val(calcPaidAmounts());
                            $('#rcvdAmount').text(calcPaidAmounts());
                        });

                        //////////////////////////////////////
                        function calcSelectedDues() {
                            var $selPOs = '';
                            $('.selCheck:checked').each(function () {
                                $checkedPO = $(this).parent().parent().find('td:eq(2)').text();
                                $selPOs += $checkedPO + ',';
                            });
                            return $selPOs;
                        };

                        function calcPaidAmounts() {
                            var $paidVal = 0;
                            $('.multiplePmts').each(function () {
                                $pmtValues = $(this).find('td:eq(1)').html();
                                $paidVal = parseFloat($paidVal) + parseFloat($pmtValues);
                            });
                            return $paidVal;
                        };
                        $('#testbtn').click(function () {
                            alert($('.multiplePmts').eq(1).css('color')); //find('.tdCQAmt').text());//child('td.tdCQAmt').child('input[name="iCQNo[]"]').val('changes');
                            //$('.multiplePmts').eq(1).child('td.tdCQAmt').text('changes');
                        });

                        function chkBalance() {
                            var $remainAmt;
                            $selDues = $('#hdInpPayableAmt').val();
                            $paidAmt = $('#hdInpPaidAmt').val();
                            $remainAmt = parseFloat($selDues) - parseFloat($paidAmt);
                            //alert($remainAmt);
                            return $remainAmt;
                        };

                        function chequeExist($curCQno) {
                            var $val;
                            $('.tdCQAmt').each(function () {
                                var $existCQ = $(this).find('input[name="iCQNo[]"]').val();
                                if ($existCQ == $curCQno) {
                                    $val = 'true';
                                    $tdInd = $(this).parent().index();
                                    return false
                                }
                                else {
                                    $val = 'false';
                                }
                            });
                            return $val;
                        };

                        function userBack() {
                            $('.multiplePmts').remove();
                            $totDues = 0;
                            $('#inpCashAmt').val('');
                            $('#inpChequeNumber').val('');
                            $('#inpChequeDate').val('');
                            $('#inpChequeBank').val('');
                            $('#inpChequeAmt').val(''); //hdInpPayableAmt  hdInpPaidAmt  hdInpSelPOs
                            $('#hdInpPayableAmt').val('0');
                            $('#hdInpPaidAmt').val('0');
                            $('#hdInpSelPOs').val('');
                            $('#selAmountShow').text('0.00');
                            $('#rcvdAmount').text('0.00');
                            //$default = '<table id="poList" class="table table-hover table-bordered"><tr><th scope="col">#</th><th scope="col">Date</th><th scope="col">P.O. Number</th><th scope="col">PO Amount</th><th scope="col">VAT</th><th scope="col">Other Charges</th><th scope="col">Total Amt.</th><th scope="col">Paid Amt.</th><th scope="col">Dues</th></tr></table>';
//	$('div#itemResult').html($default);
                        };


                    });

                </script>

            </head>

            <body>
            <!--<input type="button" id="testbtn" />-->
            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <section class="panel">
                            <header class="panel-heading">
                                Supplier Payment Form
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="col-lg-11">
                                            <?php
                                            $query = "SELECT p.supplierId, s.companyName FROM purchaseorder p LEFT JOIN suppliermaster s ON s.supplierId = p.supplierId WHERE p.status='DUE' GROUP BY p.supplierId";
                                            $result = mysql_query($query);

                                            echo ' <select id="selSupplier" name="supplier" class="form-control input-lg m-bot15 col-lg-3">';

                                            echo '<option value="#">--Select supplier--</option>';
                                            while ($row = mysql_fetch_assoc($result)) {

                                                echo '<option value="' . $row['supplierId'] . '">&nbsp;&nbsp;' . htmlspecialchars($row['companyName']) . '</option>';
                                            }

                                            echo "</select>"; ?>
                                        </div>
                                        <div class="loader" style="display:none"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div id="itemResult">
                                            <table id="poList" class="table table-hover table-bordered">
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">P.O. Number</th>
                                                    <th scope="col">PO Amount</th>
                                                    <th scope="col">VAT</th>
                                                    <th scope="col">Other Charges</th>
                                                    <th scope="col">Total Amt.</th>
                                                    <th scope="col">Paid Amt.</th>
                                                    <th scope="col">Dues</th>
                                                </tr>
                                                <!--<tr>
                                                  <td>#</td>
                                                  <td>Date</td>
                                                  <td>P.O. Number</td>
                                                  <td>PO Amount</td>
                                                  <td>VAT</td>
                                                  <td>Other Charges</td>
                                                  <td>Total Amt.</td>
                                                  <td>Paid Amt.</td>
                                                  <td>Dues</td>
                                                </tr>-->
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <!--widget start-->
                                    <header class="panel-heading">Payment Tender</header>
                                    <section class="panel">
                                        <header class="panel-heading tab-bg-dark-navy-blue">
                                            <ul class="nav nav-tabs nav-justified ">
                                                <li class="active">
                                                    <a data-toggle="tab" id="tabCash" href="#cash">
                                                        CASH
                                                    </a>
                                                </li>
                                                <li class="">
                                                    <a data-toggle="tab" id="tabCQ" href="#cheque">
                                                        CHEQUE
                                                    </a>
                                                </li>
                                                <!--<li class="">
                                                    <a data-toggle="tab" href="#recent">
                                                        Recents
                                                    </a>
                                                </li>-->
                                            </ul>
                                        </header>
                                        <div class="panel-body">
                                            <div class="tab-content tasi-tab">
                                                <div id="cash" class="tab-pane active">
                                                    <!--<form id="cashForm" name="cashForm" action="#" method="post" >-->
                                                    <div class="form-group">
                                                        <label for="inpCashAmt">CASH AMOUNT</label>
                                                        <input type="number" placeholder="0.00" id="inpCashAmt"
                                                               class="form-control">
                                                    </div>
                                                    <!--<div class="form-group">
                                                        <label for="exampleInputPassword1">Password</label>
                                                        <input type="password" placeholder="Password" id="exampleInputPassword1" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputFile">File input</label>
                                                        <input type="file" id="exampleInputFile">
                                                        <p class="help-block">Example block-level help text here.</p>
                                                    </div>-->

                                                    <button class="btn btn-info" id="btnCashPayment" type="button">Add
                                                        Payment
                                                    </button>
                                                    <!--</form>-->
                                                </div>
                                                <div id="cheque" class="tab-pane">
                                                    <div class="form-group">
                                                        <label for="inpChequeNumber">CHEQUE NUMBER</label>
                                                        <input type="number" placeholder="" id="inpChequeNumber"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inpChequeDate">CHEQUE DATE</label>
                                                        <input type="text" value="" size="16" id="inpChequeDate"
                                                               class="form-control form-control-inline input-medium default-date-picker">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inpChequeBank">CHEQUE BANK</label>
                                                        <input type="text" placeholder="" id="inpChequeBank"
                                                               class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inpChequeAmt">CHEQUE AMOUNT</label>
                                                        <input type="number" placeholder="" id="inpChequeAmt"
                                                               class="form-control">
                                                    </div>
                                                    <button class="btn btn-info" id="btnCQPayment" type="button">Add
                                                        Payment
                                                    </button>
                                                </div>
                                                <!--<div id="recent" class="tab-pane">
                                                    Recent Item goes here
                                                </div>-->
                                            </div>
                                        </div>
                                    </section>
                                    <!--widget end-->
                                </div>
                                <div class="col-lg-6">
                                    <section class="panel">
                                        <header class="panel-heading">Payments</header>
                                        <div class="panel-body">
                                            <div class="alert alert-success">
                                                <strong>Selected P.O.'s Due Value is Tk. </strong> <span
                                                        id="selAmountShow">0.00</span>
                                            </div>
                                            <form action="supplierPaymentExec.php" method="post">
                                                <input type="hidden" id="hdInpPayableAmt" name="hdInpPayableAmt"
                                                       value="0"/>
                                                <input type="hidden" id="hdInpPaidAmt" name="hdInpPaidAmt" value="0"/>
                                                <input type="hidden" id="hdInpSelPOs" name="hdInpSelPOs"/>
                                                <table id="pmtList" class="table table-hover table-bordered">
                                                    <tr>
                                                        <th scope="col">Payment Mode</th>
                                                        <th scope="col">Amount</th>
                                                    </tr>
                                                </table>
                                                <div class="alert alert-success">
                                                    <strong>Total payment added : TK.</strong> <span id="rcvdAmount">0.00</span>
                                                </div>
                                                <input type="submit" class="btn btn-success" value="Submit">
                                            </form>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </section>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>
            <!--script for this page only-->
            <!-- END JAVASCRIPTS -->


            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>