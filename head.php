<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Mosaddek">
<meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
<link rel="shortcut icon" href="img/favicon.html">

<title>Online Sales And Inventory Management System</title>

<!-- Full Screen Overlay Menu-->
<link href="css/overlay_menu.css" rel="stylesheet" />
<!-- Full Screen Overlay Menu-->

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-reset.css" rel="stylesheet">
<!--external css-->
<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
<link href="css/font-awsome_animation.min.css" rel="stylesheet" />
<link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="css/owl.carousel.css" type="text/css">

<link href="assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
<link href="assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet"/>

<script src="js/jquery.js"></script>
<script src="js/jquery-1.8.3.min.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

<script>
    function showEmployer(str) {
        $('#defaultTbl').hide();
        if (str == "") {
            document.getElementById("txtEmployer").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtEmployer").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "getEmployer.php?bid=" + str, true);
        xmlhttp.send();
    }

    function openNav(){
        $('.full-menu').toggleClass('full-menu--open');
        $('#nav-icon2').addClass('open');
    }

    function closeNav(){
        $('.full-menu').removeClass('full-menu--open');
        $('#nav-icon2').removeClass('open');
    }

    function finishAjax(id, response) {
        $('#wait_1').hide();
        $('#'+id).html(unescape(response));
        $('#'+id).fadeIn();
    }
    function finishAjax_tier_three(id, response) {
        $('#wait_2').hide();
        $('#'+id).html(unescape(response));
        $('#'+id).fadeIn();
    }

    function tot(elem) {
        var d = document.getElementById("subGTotal").value;
        var subGTotal = Number(d);

    }

    function showLoader(){
        $('div.loader').show();
    }
    function destroyLoader(){
        $('div.loader').hide();
    }

    // var subGTotal=0;
    var suTotal = 0;

    function getValues() {
        var qty1 = 0;
        var unit = 0;
        var pvat = 0;
        var ipVat = 0;
        var provat = 0;
        var obj = document.getElementsByTagName("input");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].name == "itemQty") {
                var qty1 = obj[i].value;
            }

            if (obj[i].name == "itemUnit") {
                var unit = obj[i].value;
            }
            if (obj[i].name == "itemVat") {
                var ipVat = obj[i].value;
            }
            if (obj[i].name == 'vat') {

                if (obj[i].type === 'checkbox') {

                    if (obj[i].checked) {
                        var protot = (qty1 * unit);

                        var pvat = (protot * ipVat) / 100;

                    } else {

                        var pvat = 0;
                    }
                }
            }
            if (obj[i].name == "subTotal") {
                if (qty1 > 0 && unit > 0) {
                    obj[i].value = (qty1 * unit + pvat).toFixed(2);  //  sub total
                    subGTotal += (obj[i].value * 1);   // Grand Tota
                    //suTotal+=subGTotal;
                    //alert(suTotal)
                }
                else {
                    obj[i].value = 0;
                    subGTotal += (obj[i].value * 1);
                    // alert(subGTotal+'2')

                }
            }
        }  // for loop end
    }

    function calculateSum() {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".tPrice").each(function () {

            var value = $(this).text();
            // add only if the value is number
            if (!isNaN(value) && value.length != 0) {
                sum += parseFloat(value);
                //alert(sum);
            }
            //	document.getElementById("sum").value = sum.toFixed(2);
        });
        //.toFixed() method will roundoff the final sum to 2 decimal places
        $("#subGTotal").html(sum.toFixed(2));
        //	document.getElementById("subGTotal").value = sum.toFixed(2);
        $("input[id ^='subGTotal']").val(sum.toFixed(2));
        $("input[id ^='GTotal']").val(sum.toFixed(2));
    }

    function tot(elem) {
        var d = document.getElementById("subGTotal").value;
        var subGTotal = Number(d);

    }

    var subGTotal = 0;
    var suTotal = 0;

    function getValues() {
        var qty1 = 0;
        var unit = 0;
        var pvat = 0;
        var ipVat = 0;
        var provat = 0;
        var itemDis = 0;


        var obj = document.getElementsByTagName("input");

        for (var i = 0; i < obj.length; i++) {


            if (obj[i].name == "itemQty") {
                var qty1 = obj[i].value;
            }

            if (obj[i].name == "itemUnit") {
                var unit = obj[i].value;
            }
            if (obj[i].name == "itemVat") {
                var ipVat = obj[i].value;
            }
            if (obj[i].name == "itemDis") {
                var itemDis = obj[i].value;
                //alert('fdsfsd');
            }

            //if ( obj[i].name == 'vat' )
//							{
//
//								if ( obj[i].type === 'checkbox' )
//									 {
//
//										if ( obj[i].checked )
            //	{
            //	var protot = (qty1*unit);

            //	var pvat = (protot*ipVat)/100;

            //		var itemVat = obj[i].value = 1;
            //	alert (itemVat);

            //} else {

            //	var pvat = 0;
            //	var itemVat = obj[i].value = 0;
            //	alert (itemVat);
            //}
//									 }
//							}


            if (obj[i].name == "subTotal") {
                if (qty1 > 0 && unit > 0) {
                    obj[i].value = (qty1 * unit - itemDis).toFixed(2);  //  sub total
                    subGTotal += (obj[i].value * 1);   // Grand Tota
                    //suTotal+=subGTotal;
                    //alert(suTotal)
                }
                else {
                    obj[i].value = 0;
                    subGTotal += (obj[i].value * 1);
                    // alert(subGTotal+'2')

                }
            }
        }  // for loop end
    }

    function calculateSum() {

        var sum = 0;
        //iterate through each textboxes and add the values
        $(".tPrice").each(function () {

            var value = $(this).text();
            // add only if the value is number
            if (!isNaN(value) && value.length != 0) {
                sum += parseFloat(value);
            }

            //	document.getElementById("sum").value = sum.toFixed(2);
        });

//        $("#subGTotal").html(sum.toFixed(2));
//		$("input[id ^='subGTotal']").val(sum.toFixed(2));
//		$("input[id ^='GTotal']").val(sum.toFixed(2));
    }
</script>
