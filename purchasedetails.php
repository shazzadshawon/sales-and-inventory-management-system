<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3 && $role != 5) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo


            ?>
            <!DOCTYPE html>
            <html lang="en">

            <!-- Mirrored from thevectorlab.net/flatlab/dynamic_table.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 11 Dec 2013 05:50:27 GMT -->
            <head>
                <?php require('head.php'); ?>

                <?php
                $poId = $_GET['poid'];

                $result_po = mysql_query("select * from purchaseorder where poId = '$poId'");
                $row_po = mysql_fetch_row($result_po);
                $supplierId = $row_po[1];

                $result_sup = mysql_query("select * from suppliermaster where supplierId = '$supplierId'");
                $row_sup = mysql_fetch_row($result_sup);

                $supplierName = $row_sup[1];

                $transactionDate = $row_po[2];
                $transactionTotal = $row_po[3];
                $transactionVat = $row_po[4];
                $transactionGtotal = $row_po[6];
                $paidAmount = $row_po[7];
                $dueAmount = $row_po[8];

                ?>


            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Purchase Details
                                    </header>
                                    <div class="panel-body">
                                        <?php

                                        echo '<div style="height:50px">';

                                        echo 'Supplier name : ' . $supplierName . '<br>';
                                        echo '<p style="float:right; margin-top:-30px">Purchase order id : ' . $poId . '<br>';
                                        echo 'Purchase Date : ' . $transactionDate . '<br><br></p>';
                                        echo '</div>';

                                        ?>


                                        <div class="adv-table">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Purchased Unit Price</th>
                                                    <th>Quantity</th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <?php


                                                // number of results to show per page

                                                $per_page = 50;

                                                // id 	title 	fname 	lname 	gender 	country 	district 	thana 	address 	mobile 	email 	username 	password 	introducer 	doj 	role 	dou 	status

                                                // figure out the total pages in the database


                                                // $result_prod = mysql_query("SELECT * FROM purchasedetails WHERE poId = '$poId'");
                                                //$row_prod=mysql_fetch_array($result_prod);

                                                $result = mysql_query("SELECT * FROM purchasedetails WHERE poId = '$poId' ORDER BY id DESC");


                                                $total_results = mysql_num_rows($result);

                                                $total_pages = ceil($total_results / $per_page);


                                                // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

                                                if (isset($_GET['page']) && is_numeric($_GET['page'])) {

                                                    $show_page = $_GET['page'];


                                                    // make sure the $show_page value is valid

                                                    if ($show_page > 0 && $show_page <= $total_pages) {

                                                        $start = ($show_page - 1) * $per_page;

                                                        $end = $start + $per_page;

                                                    } else {

                                                        // error - show first set of results

                                                        $start = 0;

                                                        $end = $per_page;

                                                    }

                                                } else {

                                                    // if page isn't set, show first set of results

                                                    $start = 0;

                                                    $end = $per_page;

                                                }


                                                // display data in table


                                                echo "</tbody>";


                                                // memCatId 	memCategory 	memCatValue 	comValue 	mutual_com
                                                // loop through results of database query, displaying them in the table

                                                $sum_tblOne = 0;

                                                for ($i = $start; $i < $end; $i++) {

                                                    // make sure that PHP doesn't try to show results that don't exist

                                                    if ($i == $total_results) {
                                                        break;
                                                    }


                                                    // echo out the contents of each row into a table

                                                    //jobCatid 	jobCategory 	status

                                                    echo "<tr>";


                                                    $productId = mysql_result($result, $i, 'pId');

                                                    $result_product = mysql_query("select * from product_t where id = '$productId'");
                                                    $row_product = mysql_fetch_row($result_product);
                                                    $productName = $row_product[1];

                                                    echo '<td>' . $productName . '</td>';

                                                    $price = mysql_result($result, $i, 'purchaseAmount');
                                                    $qty = mysql_result($result, $i, 'qty');
                                                    $total = $price * $qty;

                                                    $sum_tblOne = $sum_tblOne + $total;

                                                    echo '<td>' . $price . '</td>';

                                                    echo '<td>' . $qty . '</td>';

                                                    echo '<td>' . $total . '</td>';


                                                    echo "</tr>";

                                                }

                                                // close table>

                                                echo "</tbody>";

                                                echo "</table>";

                                                // pagination
                                                ////////////////////////////////////////

                                                echo '<br>';

                                                ?>



                                                <?php

                                                $result_rproduct = mysql_query("select COUNT(returnId) AS rid from returnproduct where purchaseOrderId='$poId'");
                                                $row_resultr = mysql_fetch_row($result_rproduct);
                                                $rProduct = $row_resultr[0];

                                                if ($rProduct > 0) {
                                                    echo 'Return product';
                                                    echo '<br>';
                                                    echo '<table  class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                          <th>Product Name</th>
                                          <th>Purchased Unit Price</th>
                                          <th>Quantity</th>
										  <th>Total</th>
                                      </tr>
                                      </thead>';


                                                    // number of results to show per page

                                                    $per_page = 50;

                                                    // id 	title 	fname 	lname 	gender 	country 	district 	thana 	address 	mobile 	email 	username 	password 	introducer 	doj 	role 	dou 	status

                                                    // figure out the total pages in the database


                                                    //Retrieve from return product

                                                    $result = mysql_query("SELECT * FROM returnproduct WHERE purchaseOrderId = '$poId'");


                                                    $total_results = mysql_num_rows($result);

                                                    $total_pages = ceil($total_results / $per_page);


                                                    // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

                                                    if (isset($_GET['page']) && is_numeric($_GET['page'])) {

                                                        $show_page = $_GET['page'];


                                                        // make sure the $show_page value is valid

                                                        if ($show_page > 0 && $show_page <= $total_pages) {

                                                            $start = ($show_page - 1) * $per_page;

                                                            $end = $start + $per_page;

                                                        } else {

                                                            // error - show first set of results

                                                            $start = 0;

                                                            $end = $per_page;

                                                        }

                                                    } else {

                                                        // if page isn't set, show first set of results

                                                        $start = 0;

                                                        $end = $per_page;

                                                    }


                                                    // display data in table


                                                    echo "</tbody>";


// memCatId 	memCategory 	memCatValue 	comValue 	mutual_com
                                                    // loop through results of database query, displaying them in the table
                                                    $sum_tblTwo = 0;
                                                    for ($i = $start; $i < $end; $i++) {

                                                        // make sure that PHP doesn't try to show results that don't exist

                                                        if ($i == $total_results) {
                                                            break;
                                                        }


                                                        // echo out the contents of each row into a table

                                                        //jobCatid 	jobCategory 	status

                                                        echo "<tr>";


                                                        $productId = mysql_result($result, $i, 'productId');

                                                        $result_product = mysql_query("select * from product_t where id = '$productId'");
                                                        $row_product = mysql_fetch_row($result_product);
                                                        $productName = $row_product[1];

                                                        echo '<td>' . $productName . '</td>';

                                                        $result_productPrice = mysql_query("select * from purchasedetails where pId = '$productId' AND poId= '$poId'");
                                                        $row_productPrice = mysql_fetch_row($result_productPrice);
                                                        $price = $row_productPrice[3];


                                                        $qty = mysql_result($result, $i, 'qty');


                                                        $total = $price * $qty;

                                                        $sum_tblTwo = $sum_tblTwo + $total;

                                                        echo '<td>' . $price . '</td>';

                                                        echo '<td>' . $qty . '</td>';

                                                        echo '<td>' . $total . '</td>';


                                                        echo "</tr>";

                                                    }

                                                    // close table>

                                                    echo "</tbody>";

                                                    echo "</table>";
                                                }


                                                ?>



                                                <?php

                                                $subTotal = $sum_tblOne - $sum_tblTwo;
                                                $gTotal = $subTotal + $transactionVat;
                                                echo '<div style="float:right">';
                                                echo 'Sub Total : ' . $subTotal . '<br>';
                                                echo 'VAT : ' . $transactionVat . '<br>';
                                                echo 'Total : ' . $gTotal . '<br>';
                                                echo 'Paid Amount : ' . $paidAmount . '<br>';
                                                echo 'Due Amount : ' . $dueAmount . '<br>';


                                                echo '</div>';
                                                ?>


                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php require('footer.php'); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>

            <script type="text/javascript" charset="utf-8">
                $(document).ready(function () {
                    $('#example').dataTable({
                        "aaSorting": [[4, "desc"]]
                    });
                });
            </script>
            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>