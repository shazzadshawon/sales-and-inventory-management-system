<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $path = strip_tags($get_company_details['clogo']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo

            include('func.php');

            ?>

            <!DOCTYPE html>
            <html lang="en">

            <head>
                <?php require('head.php'); ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#wait_1').hide();
                        $('#drop_1').change(function () {
                            $('#wait_1').show();
                            $('#result_1').hide();
                            $.get("func.php", {
                                func: "drop_1",
                                drop_var: $('#drop_1').val()
                            }, function (response) {
                                $('#result_1').fadeOut();
                                setTimeout("finishAjax('result_1', '" + escape(response) + "')", 400);
                            });
                            return false;
                        });
                    });

                    function finishAjax(id, response) {
                        $('#wait_1').hide();
                        $('#' + id).html(unescape(response));
                        $('#' + id).fadeIn();
                    }

                    function finishAjax_tier_three(id, response) {
                        $('#wait_2').hide();
                        $('#' + id).html(unescape(response));
                        $('#' + id).fadeIn();
                    }
                </script>
            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <?php

                $check_branch = mysql_query("select * from location where locationId = '$branchId'");
                $row_branch = mysql_fetch_row($check_branch);
                $branchName = $row_branch[1];

                // id 	locationId 	locationName 	locationAddress 	locationPhone 	locationMobile 	locationFax 	locationEmail 	locationWeb 	locationStatus
                // id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
                ?>

                <?php $uid = $_GET['id']; ?>


                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Employee Details
                                        <a href="empEdit.php?id=<?php echo $uid; ?>"><span class="btn btn-info"
                                                                                           type="button"
                                                                                           style="float:right">Update Details</span></a>

                                        <?php
                                        $result_user = mysql_query("select * from user where id = '$uid'");
                                        $row_user = mysql_fetch_row($result_user);

                                        $fname = $row_user[1];
                                        $lname = $row_user[2];
                                        $uname = $row_user[3];
                                        $pass = $row_user[4];
                                        $mobile = $row_user[5];
                                        $email = $row_user[6];
                                        $gender = $row_user[7];
                                        $address = $row_user[8];
                                        $city = $row_user[9];
                                        $country = $row_user[10];
                                        $refname = $row_user[11];
                                        $refmobile = $row_user[12];
                                        $joindate = $row_user[13];
                                        $designation = $row_user[14];
                                        $branch = $row_user[15];
                                        $company = $row_user[16];
                                        $nationalId = $row_user[17];


                                        ?>

                                    </header>
                                    <div class="panel-body">
                                        <div class=" form">
                                            <form action="" method="POST" id="commentForm"
                                                  class="cmxform form-horizontal tasi-form" novalidate>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cname">First Name
                                                        :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $fname; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Last Name
                                                        :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $lname; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="curl">Username :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $uname; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cname">Password :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $pass; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Mobile :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $mobile; ?></label>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cname">Email :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $email; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">National Id
                                                        :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $nationalId; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cname">Gender :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $gender; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cname">Address :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $address; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">City :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $city; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Country :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $country; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Reference Name
                                                        :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $refname; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Reference Mobile
                                                        :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $refmobile; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Join date
                                                        :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $joindate; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Designation
                                                        :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $designation; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Branch :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $branch; ?></label>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Company :</label>
                                                    <label class="control-label col-lg-2"
                                                           for="cname"><?php echo $company; ?></label>
                                                </div>


                                            </form>
                                        </div>

                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>
            <script>

                //step wizard

                $(function () {
                    $('#default').stepy({
                        backLabel: 'Previous',
                        block: true,
                        nextLabel: 'Next',
                        titleClick: true,
                        titleTarget: '.stepy-tab'
                    });
                });
            </script>


            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>