<?php 
    // session_start();
    ob_start(); 
?>

<div class="loader"> <img src="img/ajax-loader.gif"> </div>

<div class="sidebar-toggle-box openMenu">
<!--    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>-->
<!--    <div class="openMenu fa fa-bars faa-float animated fa-5x tooltips" onclick="openNav();"></div>-->
    <div id="nav-icon2" class="faa-float animated tooltips" onclick="openNav();">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!--logo start-->
<a href="index.php" class="logo">Sales And Inventory Management</a>
<!--logo end-->

<div class="top-nav ">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <li>
            <input type="text" class="form-control search" placeholder="Search">
        </li>
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="img/avatar1_small.jpg">
                <span class="username"> <?php echo $fname."&nbsp".$lname ?> </span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
                        <li><a href="logout.php"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
</div>