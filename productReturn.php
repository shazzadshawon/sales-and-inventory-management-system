<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $path = strip_tags($get_company_details['clogo']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo

            include('returnFunc.php');
            include('invoiceFunc.php');

            ?>

            <!DOCTYPE html>
            <html lang="en">

            <head>
                <?php require('head.php'); ?>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#divIn').hide();
                        $('#divInpro').hide();
                        $('#divPo').hide();
                        $('#divPopro').hide();

                        $('#drop_returnType').change(function () {
                            var rt = $('#drop_returnType').val();

                            if (rt == 0) {
                                $('#divPo').show();
                                $('#divPopro').show();
                                $('#divIn').hide();
                                $('#divInpro').hide();
                            } else {
                                $('#divIn').show();
                                $('#divInpro').show();
                                $('#divPo').hide();
                                $('#divPopro').hide();
                            }
                        });
                    });
                </script>


                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#wait_1').hide();
                        $('#drop_1').change(function () {
                            $('#wait_1').show();
                            $('#result_1').hide();
                            $.get("returnFunc.php", {
                                func: "drop_1",
                                drop_var: $('#drop_1').val()
                            }, function (response) {
                                $('#result_1').fadeOut();
                                setTimeout("finishAjax('result_1', '" + escape(response) + "')", 400);
                            });
                            return false;
                        });
                    });

                </script>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#wait_inv').hide();
                        $('#drop_inv').change(function () {

                            $('#wait_inv').show();
                            $('#result_inv').hide();
                            $.get("invoiceFunc.php", {
                                funcv: "drop_inv",
                                drop_varv: $('#drop_inv').val()

                            }, function (response) {
                                $('#result_inv').fadeOut();
                                setTimeout("finishAjax('result_inv', '" + escape(response) + "')", 400);
                            });
                            return false;
                        });
                    });
                </script>

                <script>
                    $(function () {
                        $("#btnSub").click(function () {

                            if ($('#rQty').val() == '') {
                                alert('Quantity is required!');
                            }
                            else {

                                var retunType = $('#drop_returnType').val();
                                if (retunType == 0) {
                                    var pid = $("select[id ^='drop_2']").val();
                                    var dataString = 'pid=' + pid;
                                } else {
                                    var pid = $("select[id ^='drop_inv_2']").val();
                                    var dataString = 'pid=' + pid;
                                }


                                $.ajax({
                                    type: "POST",
                                    url: "checkQty.php",
                                    data: dataString,
                                    success: function (data) {
                                        dbQty = data;

                                        var qty = $("#rQty").val();

                                        if (qty > dbQty) {
                                            alert('Too large value!');
                                        } else {

                                            var retunType = $('#drop_returnType').val();
                                            var qty = $("#rQty").val();
                                            var remarks = $("#remarks").val();


                                            if (retunType == 0) {
                                                var poid = $("select[id ^='drop_1']").val();
                                                var pid = $("select[id ^='drop_2']").val();
                                                var dataString2 = 'retunType=' + retunType + '&poid=' + poid + '&pid=' + pid + '&qty=' + qty + '&remarks=' + remarks;


                                            } else {
                                                var invoiceNo = $("select[id ^='drop_inv']").val();
                                                var invoicePid = $("select[id ^='drop_inv_2']").val();
                                                var dataString2 = 'retunType=' + retunType + '&invoiceNo=' + invoiceNo + '&invoicePid=' + invoicePid + '&qty=' + qty + '&remarks=' + remarks;

                                            }


                                            // alert(dataString2);

                                            $.ajax({
                                                type: "POST",
                                                url: "productReturnInc.php",
                                                data: dataString2,
                                                success: function (data) {
                                                    // alert(data);
                                                    if (data == 0) {
                                                        window.location = "purchasedetails.php?poid=" + poid;
                                                    } else {
                                                        window.location = "purchasedetails.php?poid=" + invoiceNo;
                                                    }

                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });  // button click function close
                    });  // main function close
                </script>
            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <?php

                $check_branch = mysql_query("select * from location where locationId = '$branchId'");
                $row_branch = mysql_fetch_row($check_branch);
                $branchName = $row_branch[1];


                // id 	locationId 	locationName 	locationAddress 	locationPhone 	locationMobile 	locationFax 	locationEmail 	locationWeb 	locationStatus

                // id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
                ?>


                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Product return

                                    </header>
                                    <div class="panel-body">
                                        <div class=" form">
                                            <div class="form-group ">
                                                <label class="control-label col-lg-2" for="cemail">Product Code</label>
                                                <div class="col-lg-10">
                                                    <input disabled="true" type="text" required minlength="5" maxlength="20" name="proretCode" id="proretCode"
                                                           class=" form-control" placeholder="Please Scan With Barcode Scanner">
                                                    <svg id="barcodeOutput" style="display: none"></svg>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label col-lg-2" for="cemail">Return type</label>
                                                <div class="col-lg-10">
                                                    <select name="drop_returnType" id="drop_returnType"
                                                            class="form-control">
                                                        <option value="" selected="selected" disabled="disabled">
                                                            --Select--
                                                        </option>
                                                        <option value="0">Return to vendor</option>
                                                        <option value="1">Return from customer</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="divIn" class="form-group">
                                                <label for="exampleInputEmail2" class="control-label col-lg-2">Invoice
                                                    no</label>
                                                <div class="col-lg-10">
                                                    <select name="drop_inv" id="drop_inv" class="form-control">
                                                        <option value="" selected="selected" disabled="disabled">
                                                            --Select--
                                                        </option>
                                                        <?php
                                                        getTierInvoice();

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="divInpro" class="form-group ">
                                                <label class="control-label col-lg-2" for="cemail">Invoice
                                                    product</label><br>
                                                <div class="col-lg-10">
										
										<span id="wait_inv" style="display: none;">
                                                <img alt="Please Wait" src="ajax-loader.gif"/>
                                                </span>
                                                    <span id="result_inv" style="display: none;"></span>


                                                </div>
                                            </div>


                                            <div id="divPo" class="form-group ">
                                                <label class="control-label col-lg-2" for="cemail">Purchase Order
                                                    no</label><br>
                                                <div class="col-lg-10">
                                                    <select name="drop_1" id="drop_1" class="form-control">
                                                        <option value="" selected="selected" disabled="disabled">
                                                            --Select--
                                                        </option>

                                                        <?php getTierOne(); ?>

                                                    </select>
                                                </div>
                                            </div>


                                            <div id="divPopro" class="form-group ">
                                                <label id="lblpp" class="control-label col-lg-2" for="cemail">Purchased
                                                    product</label><br>
                                                <div class="col-lg-10">
										
										<span id="wait_1" style="display: none;">
                                                <img alt="Please Wait" src="ajax-loader.gif"/>
                                                </span>
                                                    <span id="result_1" style="display: none;"></span>


                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label col-lg-2" for="cname">Quantity</label><br>
                                                <div class="col-lg-10">
                                                    <input type="text" required minlength="2" name="rQty" id="rQty"
                                                           class=" form-control">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label col-lg-2" for="cname">Remarks</label>
                                                <div class="col-lg-10">

                                                    <textarea id="remarks" name="remarks" rows="4" cols="50"
                                                              class="form-control"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="submit" class="btn btn-danger" id="btnSub"
                                                            name="btnSub">Return
                                                    </button>

                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>
            <script>
                $(function () {
                    $('#default').stepy({
                        backLabel: 'Previous',
                        block: true,
                        nextLabel: 'Next',
                        titleClick: true,
                        titleTarget: '.stepy-tab'
                    });
                });

                var node = $('#proretCode');
                var target = $(document);
                procesBarCode(node, target, '#barcodeOutput', 'inline', barcodeCallback, false);
                function barcodeCallback(data){
                    console.log(data);
                }
            </script>

            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>