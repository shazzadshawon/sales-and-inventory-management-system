<div class="full-menu">
    <div class="full-menu--middle">
        <button class="menu-toggle faa-shake faa-slow animated-hover menu-toggle--close" onclick="closeNav();"></button>

<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="overlay_item custom-3"> <a href="javascript:void(0)">Dashboard</a> </div>-->
<!--                <div class="overlay_item custom-3"> <a href="javascript:void(0)">Dashboard</a> </div>-->
<!--                <div class="overlay_item custom-3"> <a href="javascript:void(0)">Dashboard</a> </div>-->
<!--                <div class="overlay_item custom-3"> <a href="javascript:void(0)">Dashboard</a> </div>-->
<!--            </div>-->
<!--        </div>-->

        <div class="container">
            <div class="row full-height overlay_row">
                <a class="overlay_container ovmenref" href="javascript:void(0);" clickTo="index.php">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item single_vAlign">
                        <div class="single_vAlign_cell h_align">
                            <span class="os_item">Dashboard</span>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" class="flipCard overlay_container">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item">
                        <div class="front v_align h_align">
                            <div class="menu_inner_item">
                                <span class="os_item">Company Setting</span>
                            </div>
                        </div>
                        <div class="back h_align">
                            <div class="inner_banner">Company Setting</div>
                            <div class="menu_inner_item">
                                <div class="list-group">
                                    <span class="list-group-item ovmenref" clickTo="companyProfile.php">Company Details</span>
                                    <span class="list-group-item ovmenref" clickTo="companyBranch.php">Branch Details</span>
                                    <span class="list-group-item ovmenref" clickTo="employer.php">Employer List</span>
                                    <span class="list-group-item ovmenref" clickTo="setInvoiceFormat.php">Set Invoice</span>
                                    <span class="list-group-item ovmenref" clickTo="settingVat.php">Setting</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" class="flipCard overlay_container">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item">
                        <div class="front v_align h_align">
                            <div class="menu_inner_item">
                                <span class="os_item">Customer Details</span>
                            </div>
                        </div>
                        <div class="back h_align">
                            <div class="inner_banner">Customer Details</div>
                            <div class="menu_inner_item">
                                <div class="list-group">
                                    <span class="list-group-item ovmenref" clickTo="customerAdd.php">Add Customer</span>
                                    <span class="list-group-item ovmenref" clickTo="customerMaster.php">Customer List</span>
                                    <span class="list-group-item ovmenref" clickTo="customerLedger.php">Customer Ladger</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" class="flipCard overlay_container">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item">
                        <div class="front v_align h_align">
                            <div class="menu_inner_item">
                                <span class="os_item">Supplier Details</span>
                            </div>
                        </div>
                        <div class="back h_align">
                            <div class="inner_banner">Supplier Details</div>
                            <div class="menu_inner_item">
                                <div class="list-group">
                                    <span class="list-group-item ovmenref" clickTo="supplierAdd.php">Add Supplier</span>
                                    <span class="list-group-item ovmenref" clickTo="supplierMaster.php">Supplier List</span>
                                    <span class="list-group-item ovmenref" clickTo="supplyLedger.php">Supplier Ladger</span>
                                    <span class="list-group-item ovmenref" clickTo="supplierPayment.php">Supplier Payment</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="row full-height overlay_row">
                <a href="javascript:void(0)" class="flipCard overlay_container">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item">
                        <div class="front v_align h_align">
                            <div class="menu_inner_item">
                                <span class="os_item">Products Details</span>
                            </div>
                        </div>
                        <div class="back h_align">
                            <div class="inner_banner">Products Details</div>
                            <div class="menu_inner_item">
                                <div class="list-group">
                                    <span class="list-group-item ovmenref" clickTo="category.php">Category</span>
                                    <span class="list-group-item ovmenref" clickTo="subCategory.php">Sub Category</span>
                                    <span class="list-group-item ovmenref" clickTo="brand.php">Brand</span>
                                    <span class="list-group-item ovmenref" clickTo="productAdd.php">Add Product</span>
                                    <span class="list-group-item ovmenref" clickTo="productMaster.php">Product Master</span>
                                    <span class="list-group-item ovmenref" clickTo="allproductPolicy.php">Product Policy</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" class="flipCard overlay_container">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item">
                        <div class="front v_align h_align">
                            <div class="menu_inner_item">
                                <span class="os_item">Purchase</span>
                            </div>
                        </div>
                        <div class="back h_align">
                            <div class="inner_banner">Purchase</div>
                            <div class="menu_inner_item">
                                <div class="list-group">
                                    <span class="list-group-item ovmenref" clickTo="purchaseOrder.php">Purchase Product</span>
                                    <span class="list-group-item ovmenref" clickTo="pph.php">Purchase Product History</span>
                                    <span class="list-group-item ovmenref" clickTo="productReturn.php">Return Product</span>
                                    <span class="list-group-item ovmenref" clickTo="returnProductList.php">Return Product List</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" class="flipCard overlay_container">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item">
                        <div class="front v_align h_align">
                            <div class="menu_inner_item">
                                <span class="os_item">Sales</span>
                            </div>
                        </div>
                        <div class="back h_align">
                            <div class="inner_banner">Sales</div>
                            <div class="menu_inner_item">
                                <div class="list-group">
                                    <span class="list-group-item ovmenref" clickTo="invoice.php">New Invoice</span>
                                    <span class="list-group-item ovmenref" clickTo="invoiceList.php" >Invoice List</span>
                                    <span class="list-group-item ovmenref" clickTo="productReturnSales.php"">Return Product</span>
                                    <span class="list-group-item" >Credit Payment</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="javascript:void(0)" class="flipCard overlay_container">
                    <div class="col-md-3 col-sm-3 col-xs-12 col-xl-4 overlay_item">
                        <div class="front v_align h_align">
                            <div class="menu_inner_item">
                                <span class="os_item">Report</span>
                            </div>
                        </div>
                        <div class="back h_align">
                            <div class="inner_banner">Report</div>
                            <div class="menu_inner_item">
                                <div class="list-group">
                                    <span class="list-group-item ovmenref" clickTo="purchaseLedger.php">Purchase Report</span>
                                    <span class="list-group-item ovmenref" clickTo="stockDetails.php">Inventory Report</span>
                                    <span class="list-group-item ovmenref" clickTo="salesReport.php">Sales Report</span>
                                    <span class="list-group-item ovmenref" clickTo="accpayable.php">Accounts Payable</span>
                                    <span class="list-group-item ovmenref" clickTo="accreceiveable.php">Account Receiveable</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <!-- /.main nav -->
    </div>
    <!-- /.full menu middle -->
</div>
<!-- /.full menu -->