<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3 && $role != 5) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id   userFname   userLname   userId  password  mobileNo  emailId   gender  address   city  country   refName   refMobile   joinDate  designation   barnchId  companyId   nationalId  role  status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //  $companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo


            ?>
            <!DOCTYPE html>
            <html lang="en">

            <!-- Mirrored from thevectorlab.net/flatlab/dynamic_table.html by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 11 Dec 2013 05:50:27 GMT -->
            <head>
                <?php require('head.php'); ?>
            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Sales Report
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                            <table class="display table table-bordered table-striped" id="example">
                                                <thead>
                                                <tr>
                                                    <th>Customer</th>
                                                    <th>Invoice No</th>


                                                    <th>Total Amount</th>

                                                    <th>Due Amount</th>

                                                    <th>Invoice Date</th>
                                                </tr>
                                                </thead>
                                                <?php


                                                // number of results to show per page

                                                $per_page = 50;

                                                // id   title   fname   lname   gender  country   district  thana   address   mobile  email   username  password  introducer  doj   role  dou   status

                                                // figure out the total pages in the database

                                                $result = mysql_query("SELECT invoicemaster.invoiceId,invoicemaster.invoiceNo, invoicemaster.invoiceDate, invoicemaster.creditAmount, invoicemaster.invoiceTotal, invoicemaster.vatExclusive,invoicemaster.discountAmount,invoicemaster.grandTotal, customermaster.customerName  FROM invoicemaster LEFT JOIN customermaster ON invoicemaster.customerId=customermaster.customerId ORDER BY invoicemaster.invoiceId DESC");

                                                $total_results = mysql_num_rows($result);

                                                $total_pages = ceil($total_results / $per_page);


                                                // check if the 'page' variable is set in the URL (ex: view-paginated.php?page=1)

                                                if (isset($_GET['page']) && is_numeric($_GET['page'])) {

                                                    $show_page = $_GET['page'];


                                                    // make sure the $show_page value is valid

                                                    if ($show_page > 0 && $show_page <= $total_pages) {

                                                        $start = ($show_page - 1) * $per_page;

                                                        $end = $start + $per_page;

                                                    } else {

                                                        // error - show first set of results

                                                        $start = 0;

                                                        $end = $per_page;

                                                    }

                                                } else {

                                                    // if page isn't set, show first set of results

                                                    $start = 0;

                                                    $end = $per_page;

                                                }


                                                // display data in table


                                                echo "</tbody>";


                                                // memCatId   memCategory   memCatValue   comValue  mutual_com
                                                // loop through results of database query, displaying them in the table

                                                for ($i = $start; $i < $end; $i++) {

                                                    // make sure that PHP doesn't try to show results that don't exist

                                                    if ($i == $total_results) {
                                                        break;
                                                    }


                                                    // echo out the contents of each row into a table

                                                    //jobCatid  jobCategory   status

                                                    echo "<tr>";

                                                    //trackId,username,investAmount
                                                    echo '<td>' . mysql_result($result, $i, 'customerName') . '</td>';
                                                    echo '<td>' . mysql_result($result, $i, 'invoiceNo') . '</td>';


                                                    echo '<td>' . mysql_result($result, $i, 'invoiceTotal') . '</td>';

                                                    echo '<td>' . mysql_result($result, $i, 'creditAmount') . '</td>';


                                                    echo '<td>' . mysql_result($result, $i, 'invoiceDate') . '</td>';


                                                    echo "</tr>";

                                                }

                                                // close table>

                                                echo "</tbody>";

                                                echo "</table>";

                                                // pagination


                                                ?>


                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <!--footer start-->
                <?php include('footer.php') ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>

            <script type="text/javascript" charset="utf-8">
                $(document).ready(function () {
                    $('#example').dataTable({
                        "aaSorting": [[4, "desc"]]
                    });
                });
            </script>
            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>