<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $path = strip_tags($get_company_details['clogo']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo


            ?>

            <!DOCTYPE html>
            <html lang="en">

            <head>
                <?php require('head.php'); ?>
            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <?php
                $branchId = $_GET['bid'];
                $check_branch = mysql_query("select * from location where locationId = '$branchId'");
                $row_branch = mysql_fetch_row($check_branch);
                $branchName = $row_branch[1];


                // id 	locationId 	locationName 	locationAddress 	locationPhone 	locationMobile 	locationFax 	locationEmail 	locationWeb 	locationStatus

                // id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status


                ?>
                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Add Employer for <?php echo $branchName; ?> Branch
                                    </header>
                                    <div class="panel-body">
                                        <div class="stepy-tab">
                                            <ul id="default-titles" class="stepy-titles clearfix">
                                                <li id="default-title-0" class="current-step">
                                                    <div>User Details</div>
                                                </li>
                                                <li id="default-title-1" class="">
                                                    <div>Personal Details</div>
                                                </li>
                                                <li id="default-title-2" class="">
                                                    <div>Office Details</div>
                                                </li>
                                            </ul>
                                        </div>
                                        <form class="form-horizontal" id="default" action="empIns.php" method="post">
                                            <fieldset title="Step1" class="step" id="default-step-0">
                                                <legend></legend>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">First Name</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="userFname"
                                                               placeholder="First Name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Last Name</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="userLname"
                                                               placeholder="Last Name">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Gender</label>
                                                    <div class="col-lg-10">
                                                        <select name="gender" class="form-control m-bot15">
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Mobile No</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="mobileNo"
                                                               placeholder="Mobile No">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Address</label>
                                                    <div class="col-lg-10">
                                                        <textarea name="address" class="form-control" cols="60"
                                                                  rows="5"></textarea>
                                                    </div>
                                                </div>


                                            </fieldset>
                                            <fieldset title="Step 2" class="step" id="default-step-1">
                                                <legend></legend>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">User Name</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="userId"
                                                               placeholder="Username">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Password</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="password"
                                                               placeholder="Password">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Confirm Password</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="cpassword"
                                                               placeholder="Confirm Password">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Email</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="emailId"
                                                               placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Branch</label>
                                                    <div class="col-lg-10">

                                                        <input type="text" class="form-control" name="branch"
                                                               value="<?php echo $branchName; ?>">

                                                    </div>
                                                </div>

                                            </fieldset>
                                            <fieldset title="Step 3" class="step" id="default-step-2">
                                                <legend></legend>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Designation</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="designation"
                                                               placeholder="Mobile">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Join Date</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" name="joinDate" size="16"
                                                               placeholder="Join Date"
                                                               class="form-control form-control-inline input-medium default-date-picker">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Refference Name</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="refName"
                                                               placeholder="Refference Name">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Refference Mobile</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" name="refMobile"
                                                               placeholder="Refference Mobile">
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <input type="submit" class="finish btn btn-danger" value="Finish"/>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>

            <script>
                $(function () {
                    $('#default').stepy({
                        backLabel: 'Previous',
                        block: true,
                        nextLabel: 'Next',
                        titleClick: true,
                        titleTarget: '.stepy-tab'
                    });
                });
            </script>

            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>