<script>

    //owl carousel

    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            autoPlay: true

        });
    });

    //custom select box

    $(function () {
        $('select.styled').customSelect();
    });

    //    var node = $('#barcodeScanner');
    //    var target = $(document);
    //    procesBarCode(node, target, '#barcodeOutput');
</script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').dataTable({
            "aaSorting": [[4, "desc"]],
            "lengthMenu": [10, 25, 50, "All"]
        });
    });
    $(window).load(function(){
        $('#example_length').append('<buttton style="margin-left: 20px" type="button" onclick="printTable()" class="btn btn-info btn-xs">Print Table</buttton>');
        $('#example_length').append('<buttton style="margin-left: 20px" type="button" onclick="clearSearch()" class="btn btn-danger btn-xs">Clear search</buttton>');
//                    $('.dataTables_filter').html('<span> <div class="input-group">\n' +
//                        '<input id="tableSearchBar" type="text" class="form-control" aria-controls="example" placeholder="Type to Search OR scan with scanner">\n' +
//                        '<span class="input-group-btn">\n' +
//                        '<button id="printBarCode" onclick="clearSearch()" class="btn btn-default" type="button">Clear</button>\n' +
//                        '</span>\n' +
//                        '</div> </span>');

        var node = $('#example_filter input');
        var target = $(document);
        procesBarCode(node, target, '', '', barcodeCallback, true);
    });
    function barcodeCallback(data){}
    function printTable(){
        console.log("printing Table");
        printData('example');
    }
    function clearSearch(){
        $('#example_filter input').val('');
    }
</script>