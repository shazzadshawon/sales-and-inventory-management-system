<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
//$user= $_SESSION['SESS_MEMBER_ID'];
//Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
//Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
//echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
//echo $role;
        if ($role != 3 && $role != 5) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
//echo 'Authorised';echo '<br>';

//Get all the logged in user information from the database users table
//$get_user_details = mysql_fetch_array($check_user_details);
//echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

//	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo
            $invoiceId = $_GET['invoiceId'];
            $invoiceNo = $_GET['invoiceNo'];


            ?>

            <!DOCTYPE html>
            <html lang="en">
            <head>
                <?php require('head.php'); ?>
                <link href="css/invoice-print.css" rel="stylesheet" media="print">

                <style>
                    .outside_border{
                        border: 1px solid #c7c7c7;
                    }
                    .position_water{
                        position: absolute;
                        right: 3%;
                        bottom: 0;
                        color: #cccccc;
                        font-size: 13px;
                    }
                    .block {
                        background-color: #ffffff;
                        width: 200px;
                        margin: auto;
                        padding: 30px 0;
                    }
                    .ribbon {
                        box-sizing: border-box;
                        position: relative;
                        margin: 20px;
                        height: 20px;
                        width: 110px;
                        color: #afafaf;
                        font-size: 18px;
                        text-align: center;
                        line-height: 20px;
                        padding: 0 10px;
                        background-color: #97c7fa78;
                        top: 0;
                        left: 0;
                    }

                    .shadow {
                        box-sizing: border-box;
                        overflow: visible;
                    }

                    .fold {
                        box-sizing: border-box;
                        position: absolute;
                        left: 0;
                        width: 0;
                        height: 0;

                        border-style: solid;
                    }

                    .a4size{
                        height: 842px;
                        width: 595px;
                        margin-left: auto;
                        margin-right: auto;
                    }
                </style>
                <script type="text/javascript">
                    var printelem = '?invoiceId=120&invoiceNo=shazzad-28-11-2017-20-48';
                    var printServer = 'http://localhost/sipm/';
                    console.log(location.href);
                </script>
            </head>

            <body>

            <section id="container" class="">
                <!--header start-->
                <?php require('header.php'); ?>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <?php
                $result = mysql_query("SELECT * FROM invoicedetails WHERE invoiceId='$invoiceId'");
                $productId = mysql_result($result, 0, 'productId');
                $qty = mysql_result($result, 0, 'qty');
                $unitePrice = mysql_result($result, 0, 'unitePrice');
                $discount = mysql_result($result, 0, 'discount');
                $vat = mysql_result($result, 0, 'vat');
                $total = mysql_result($result, 0, 'total');
                $status = mysql_result($result, 0, 'status');

                $invoice = mysql_query("SELECT * FROM invoicemaster WHERE invoiceId='$invoiceId' AND invoiceNo='$invoiceNo'");
                $invoiceDate = mysql_result($invoice, 0, 'invoiceDate');
                $customerId = mysql_result($invoice, 0, 'customerId');
                $grandTotal = mysql_result($invoice, 0, 'grandTotal');

                $invoicecustomer = mysql_query("SELECT * FROM customermaster WHERE customerId='$customerId'");
                $customerName = mysql_result($invoicecustomer, 0, 'customerName');
                $designation = mysql_result($invoicecustomer, 0, 'designation');
                $companyName = mysql_result($invoicecustomer, 0, 'companyName');
                $address = mysql_result($invoicecustomer, 0, 'address');
                $phoneNo = mysql_result($invoicecustomer, 0, 'phoneNo');
                $emailId = mysql_result($invoicecustomer, 0, 'emailId');
                ?>


                <!--main content start-->
                <section id="main-content" class="container">
                    <section class="wrapper">
                        <!-- invoice start-->
                        <section>
                            <div class="panel panel-primary">
                                <!--<div class="panel-heading navyblue"> INVOICE</div>-->
                                <div class="panel-body">
                                    <div class="row invoice-list">
                                        <div class="text-center corporate-id">
                                            <img src="img/agvLogofb.png" alt="">
                                        </div>
                                    </div>
                                    <div class="container-fluid outside_border">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Item</th>
                                                <th class="hidden-phone">Description</th>
                                                <th class="">Unit Cost</th>
                                                <th class="">Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>

                                            <?php
                                            $per_page = 50;
                                            $total_results = mysql_num_rows($result);
                                            $total_pages = ceil($total_results / $per_page);
                                            if (isset($_GET['page']) && is_numeric($_GET['page'])) {
                                                $show_page = $_GET['page'];
                                                if ($show_page > 0 && $show_page <= $total_pages) {
                                                    $start = ($show_page - 1) * $per_page;
                                                    $end = $start + $per_page;
                                                } else {
                                                    $start = 0;
                                                    $end = $per_page;
                                                }
                                            } else {
                                                $start = 0;
                                                $end = $per_page;
                                            }
                                            echo "</tbody>";
                                            for ($i = $start; $i < $end; $i++) {
                                                if ($i == $total_results) {
                                                    break;
                                                }
                                                echo "<tr>";
                                                $result_product = mysql_query("select * from product_t where id = '$productId'");
                                                $row_product = mysql_fetch_row($result_product);
                                                $productName = $row_product[1];
                                                echo '<td>' . $productName . '</td>';
                                                echo '<td>' . mysql_result($result, $i, 'unitPrice') . '</td>';
                                                echo '<td>' . mysql_result($result, $i, 'qty') . '</td>';
                                                echo '<td>' . mysql_result($result, $i, 'discount') . '</td>';
                                                echo '<td>' . mysql_result($result, $i, 'vat') . '</td>';
                                                echo '<td>' . mysql_result($result, $i, 'total') . '</td>';
                                                // echo '<td>
                                                //                 <span class="btn btn-success"><li><a  href="invoicePrint.php?invNo='.mysql_result($result, $i, 'invoiceNo') . '">Print</a></li></span>
                                                //            </td>';
                                                echo "</tr>";
                                            }
                                            echo "</tbody>";
                                            ?>

                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 invoice-block pull-right outside_border">
                                            <ul class="unstyled amounts">
                                                <li><strong>Sub - Total amount :</strong> &#2547;<?php echo $total; ?></li>
                                                <li><strong>Discount :</strong> <?php echo $discount; ?>%</li>
                                                <li><strong>VAT :</strong> <?php echo $vat; ?></li>
                                                <li><strong>Grand Total :</strong> &#2547;<?php echo $grandTotal; ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- invoice end-->
                    </section>
                </section>
                <!--main content end-->

                <!--footer start-->
                <?php require('footer.php'); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>
            <script type="text/javascript">
                (function($) {
                    $.ribbon = function (options) {
                        var settings = $.extend({
                            'this': $('.ribbon'),
                            'side': 'left',
                            'color': '#333',
                            'width': 10,
                            'height': 10
                        }, options);


                        settings.this.each(function () {
                            var that = $(this);

                            that.wrap('<div class="shadow">');
                            that.append('<div class="fold"></div>');
                            that.css({'margin-left': -settings.width});
                            that.parent().css({'width': that.width() - settings.width});

                            $(document).on('scroll', function () {
                                var min = $(document).scrollTop();
                                //var max = min + $(window).height();
                                var max = min + window.innerHeight;
                                var mid = (max + min) / 2;
                                var range = (max - min) / 2;
                                var top = that.offset().top;
                                var bot = top + that.height();
                                var offset;
                                if (top < mid) {
                                    offset = top - min - range;
                                } else if (bot > mid) {
                                    offset = bot - min - range;
                                } else {
                                    return;
                                }
                                var scale = (offset / range);
                                var height = Math.floor(scale  * settings.height);
                                var fold = that.find('.fold');
                                var shadow = that.parent();
                                console.log(height);
                                if (scale < 0) {
                                    fold.css({
                                        'top': '',
                                        'bottom': height,
                                        'border-width': Math.abs(height)+'px 0 0 '+settings.width+'px',
                                        'border-color': settings.color+' transparent transparent transparent'
                                    });
                                } else {
                                    fold.css({
                                        'top': -height,
                                        'bottom': '',
                                        'border-width': '0 0 '+Math.abs(height)+'px '+settings.width+'px',
                                        'border-color': 'transparent transparent '+settings.color+' transparent'});
                                }

                                shadow.css({ boxShadow:
                                '0 '+
                                -height+'px '+//-scale*4+'px '+
                                '2px '+
                                '#888'
                                });
                            });
                        });
                    }
                    $.fn.ribbon = function (options) {
                        var args = $.extend({
                            'this': this,
                        }, options);
                        $.ribbon(args);
                        return this;
                    }
                })( jQuery );

                $(document).ready(function () {
                    $.ribbon();
                    $(document).trigger('scroll');
                });
            </script>

            </body>
            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>