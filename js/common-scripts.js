/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
//        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
});

var Script = function () {

//    sidebar dropdown menu auto scrolling

    jQuery('#sidebar .sub-menu > a').click(function () {
        var o = ($(this).offset());
        diff = 250 - o.top;
        if(diff>0)
            $("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            $("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

//    sidebar toggle

    // $(function() {
    //     function responsiveView() {
    //         var wSize = $(window).width();
    //         if (wSize <= 768) {
    //             $('#container').addClass('sidebar-close');
    //             $('#sidebar > ul').hide();
    //         }
    //
    //         if (wSize > 768) {
    //             $('#container').removeClass('sidebar-close');
    //             $('#sidebar > ul').show();
    //         }
    //     }
    //     $(window).on('load', responsiveView);
    //     $(window).on('resize', responsiveView);
    // });

    // $('.fa-bars').click(function () {
    //     if ($('#sidebar > ul').is(":visible") === true) {
    //         $('#main-content').css({
    //             'margin-left': '0px'
    //         });
    //         $('#sidebar').css({
    //             'margin-left': '-210px'
    //         });
    //         $('#sidebar > ul').hide();
    //         $("#container").addClass("sidebar-closed");
    //     } else {
    //         $('#main-content').css({
    //             'margin-left': '210px'
    //         });
    //         $('#sidebar > ul').show();
    //         $('#sidebar').css({
    //             'margin-left': '0'
    //         });
    //         $("#container").removeClass("sidebar-closed");
    //     }
    // });

// custom scrollbar
    $("#sidebar").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});

    $("html").niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '6', cursorborderradius: '10px', background: '#404040', spacebarenabled:false,  cursorborder: '', zindex: '1000'});

// widget tools

    jQuery('.panel .tools .fa-chevron-down').click(function () {
        var el = jQuery(this).parents(".panel").children(".panel-body");
        if (jQuery(this).hasClass("fa-chevron-down")) {
            jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    jQuery('.panel .tools .fa-times').click(function () {
        jQuery(this).parents(".panel").parent().remove();
    });


//    tool tips

    $('.tooltips').tooltip();

//    popovers

    $('.popovers').popover();



// custom bar chart

    if ($(".custom-bar-chart")) {
        $(".bar").each(function () {
            var i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

}();

$.ajaxSetup({
    beforeSend: function() {
        showLoader();
    },
    complete: function() {
        destroyLoader();
    }
});

$(window).load(function(){

    $('.ovmenref').click(function(){
       var reff = $(this).attr('clickTo');
       closeNav();
       window.location.href = reff;
    });

    var DELAY = 700,
        clicks = 0,
        timer = null;

    // $('.overlay_container').live("click", function(e){
    //     clicks++;
    //     if(clicks === 1) {
    //         timer = setTimeout(function() {
    //             // alert('Single Click');
    //             clicks = 0;
    //         }, DELAY);
    //     } else {
    //         clearTimeout(timer);
    //         // alert('Double Click');
    //         clicks = 0;
    //     }
    // }).live("dblclick", function(e){
    //     e.preventDefault();
    // });

    $('.flipCard').flip({
        axis: 'y',
        speed: 150,
        trigger: 'hover'
    }).on('flip:done', function(){
        var h = $(this).find('.back').find('.menu_inner_item').height();
        if(h > 250){
            $(this).find('.overlay_item').niceScroll({styler:"fb",cursorcolor:"#e8403f", cursorwidth: '3', cursorborderradius: '10px', background: '#404040', spacebarenabled:false, cursorborder: ''});
        }
    });

    $(document).keyup(function(e){
        if(e.keyCode == 27){
            if ($('.full-menu').hasClass('full-menu--open')){
                console.log('Going to close the Menu');
                closeNav();
            }
        }
    });
});

function procesBarCode(node, target, drawTarget, drawMode, callbackfunction, isfocus){
    target.scannerDetection({
        timeBeforeScanTest: 200,
        startChar: [120],
        endChar: [13],
        avgTimeByChar: 40,
        scanButtonLongPressThreshold: 3,
        onScanButtonLongPressed: longPressCallback,
        onComplete: function(barcode, qty){
            if(isfocus){
                node.focus();
            }
            node.val(barcode);
            if(drawTarget != ''){
                console.log('Drawing Barcode');
                createBarCode(drawTarget, barcode, drawMode);
            }
            callbackfunction(barcode);
        }
    });
}

function longPressCallback(){
    alert('You have been Pressing the scan Key for Too Long');
}

function createBarCode(node, input, mode){
    if(input.length >= 30){
        alert('Please reduce your Barcode Value bellow 30 charecter');
    } else if(input.length == 0){
        // console.log('No Data Detected to create Barcode');
        $(node).hide();
    } else {
        $(node).show();
        var lineColor = "#5b6e84";
        var background = "transparent";
        var textPosition = "top";
        var displayValue = true;
        var barheight = 150;
        var width = 1;
        switch(mode) {
            case 'basic':

                break;
            case 'inline':
                lineColor = "#5b6e84";
                background = "#ccffff";
                textPosition = "bottom";
                displayValue = true;
                barheight = 30;
                break;
            case 'gridView':
                lineColor = "#000";
                displayValue = false;
                barheight = 40;
                width = 1;
                break;
            case 'print':
                lineColor = "#000";
                background = "transparent";
                textPosition = "bottom";
                displayValue = false;
                barheight = 50;
                break;
            default:

        }
        JsBarcode(node, input, {
            lineColor: lineColor,
            background: background,
            textPosition: textPosition,
            displayValue: displayValue,
            font: "OCR-B",
            fontSize: "13px",
            height: barheight,
            width: width
        });
    }
}

function printData(nodeName) {
    var divToPrint = document.getElementById(nodeName);
    newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}

function printInvoice(url){
    console.log(url);
    var printWindow = window.open( url, 'Print', 'left=200, top=200, width=950, height=500, toolbar=0, resizable=0');
    printWindow.addEventListener('load', function(){
        printWindow.print();
        printWindow.close();
    }, true);

    return true;
}