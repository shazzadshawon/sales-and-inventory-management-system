<?php
session_start();
ob_start();

//Include the database connection file
include "config.php";

//Check to be sure that a valid session has been created
if (isset($_SESSION['SESS_MEMBER_ID'])) {
    //$user= $_SESSION['SESS_MEMBER_ID'];
    //Check the database table for the logged in user information
    $check_user_details = mysql_query("select * from user where userId = '" . mysql_real_escape_string($_SESSION["SESS_MEMBER_ID"]) . "'");
    //Validate created session
    if (mysql_num_rows($check_user_details) < 1) {
        //echo 'Not in Member List';echo '<br>';
        session_unset();
        session_destroy();
        header("location: login.php");
    } elseif (mysql_num_rows($check_user_details) > 0) {
        //echo 'Member';echo '&nbsp;&nbsp;';
        $get_user_details = mysql_fetch_array($check_user_details);
        $role = strip_tags($get_user_details['role']);
        //echo $role;
        if ($role != 3) {
            //echo 'But Not Authorised';echo '<br>';
            header("location: error.php");
            exit();
        } else {
            //echo 'Authorised';echo '<br>';

            //Get all the logged in user information from the database users table
            //$get_user_details = mysql_fetch_array($check_user_details);
            //echo $get_user_details;  id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
            require_once('auth.php');
            $fname = strip_tags($get_user_details['userFname']);
            $lname = strip_tags($get_user_details['userLname']);
            $mobileNo = strip_tags($get_user_details['mobileNo']);
            $userId = strip_tags($get_user_details['userId']);

            $role = strip_tags($get_user_details['role']);
            $barnchId = strip_tags($get_user_details['barnchId']);
            $companyId = strip_tags($get_user_details['companyId']);


            $check_company_details = mysql_query("select * from company");
            $get_company_details = mysql_fetch_array($check_company_details);

            $companyName = strip_tags($get_company_details['companyName']);
            $ownerName = strip_tags($get_company_details['ownerName']);
            $phoneNo = strip_tags($get_company_details['phoneNo']);
            $regNo = strip_tags($get_company_details['regNo']);
            $mobileNo = strip_tags($get_company_details['mobileNo']);

            $faxNo = strip_tags($get_company_details['faxNo']);
            $address = strip_tags($get_company_details['address']);
            $city = strip_tags($get_company_details['city']);
            $country = strip_tags($get_company_details['country']);
            $path = strip_tags($get_company_details['clogo']);
            $comEmail = strip_tags($get_company_details['comEmail']);
            $comWeb = strip_tags($get_company_details['comWeb']);

            //	$companyName $ownerName $phoneNo $mobileNo $faxNo $address $city $country $comEmail $comWeb $regNo

            include('func.php');

            //Receiving product id from proMasterInc
            $productId = $_GET['pid'];
            ?>

            <!DOCTYPE html>
            <html lang="en">

            <head>
                <?php require('head.php'); ?>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#wait_1').hide();
                        $('#drop_1').change(function () {
                            $('#wait_1').show();
                            $('#result_1').hide();
                            $.get("func.php", {
                                func: "drop_1",
                                drop_var: $('#drop_1').val()
                            }, function (response) {
                                $('#result_1').fadeOut();
                                setTimeout("finishAjax('result_1', '" + escape(response) + "')", 400);
                            });
                            return false;
                        });
                    });
                </script>
            </head>

            <body>

            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php include("header.php"); ?>
                </header>
                <!--header end-->

                <!--Overlay start-->
                <div><?php require("overlayMenu.php"); ?></div>
                <!--Overlay end-->

                <!--main content start-->
                <?php

                $check_branch = mysql_query("select * from location where locationId = '$branchId'");
                $row_branch = mysql_fetch_row($check_branch);
                $branchName = $row_branch[1];


                // id 	locationId 	locationName 	locationAddress 	locationPhone 	locationMobile 	locationFax 	locationEmail 	locationWeb 	locationStatus

                // id 	userFname 	userLname 	userId 	password 	mobileNo 	emailId 	gender 	address 	city 	country 	refName 	refMobile 	joinDate 	designation 	barnchId 	companyId 	nationalId 	role 	status
                ?>


                <section id="main-content">
                    <section class="wrapper site-min-height">
                        <!-- page start-->
                        <div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Vat Setting
                                        <?php
                                        $result = mysql_query("select * from setting");
                                        $row_result = mysql_fetch_row($result);
                                        $VatType = $row_result[0];
                                        $mvat = $row_result[1];
                                        $invoiceType = $row_result[2];
                                        ?>


                                    </header>
                                    <div class="panel-body">
                                        <div class=" form">
                                            <form action="settingVatInc.php" method="POST" id="commentForm"
                                                  class="cmxform form-horizontal tasi-form">

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Product vat
                                                        type</label>
                                                    <div class="col-lg-10">
                                                        <select name="vatType" id="vatType" class="form-control">
                                                            <option value="0" <?php if ($VatType == 0) echo 'selected="selected"'; ?>>
                                                                Inclusive
                                                            </option>
                                                            <option value="1" <?php if ($VatType == 1) echo 'selected="selected"'; ?>>
                                                                Exclusive
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cemail">Master
                                                        vat</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" value="<?php echo $mvat; ?>" required
                                                               minlength="2" name="mvat" id="mvat"
                                                               class="form-control ">
                                                    </div>
                                                </div>

                                                <div class="form-group ">
                                                    <label class="control-label col-lg-2" for="cname">Invoice
                                                        type</label>
                                                    <div class="col-lg-10">
                                                        <select name="invoiceType" id="invoiceType"
                                                                class="form-control">
                                                            <option value="0" <?php if ($invoiceType == 0) echo 'selected="selected"'; ?>>
                                                                Single
                                                            </option>
                                                            <option value="1" <?php if ($invoiceType == 1) echo 'selected="selected"'; ?>>
                                                                Vatwise
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <button type="submit" class="btn btn-danger">Save</button>
                                                    </div>
                                                </div>


                                            </form>
                                        </div>

                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- page end-->
                    </section>
                </section>
                <!--main content end-->
                <?php include("footer.php"); ?>
                <!--footer end-->
            </section>

            <?php require('foot.php'); ?>

            <script>
                //step wizard
                $(function () {
                    $('#default').stepy({
                        backLabel: 'Previous',
                        block: true,
                        nextLabel: 'Next',
                        titleClick: true,
                        titleTarget: '.stepy-tab'
                    });
                });
            </script>


            </body>

            </html>
            <?php
        }
    }

} else {
    header("location: login.php");
    exit();
}

?>